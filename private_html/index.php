<?php

/**
 * Application Bootstrap Script
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

//Application directory location.
define("__TESTING__", true);

if(defined("__TESTING__") && (constant("__TESTING__") == TRUE)) {
	define("__APPDIR__", "/srv/www/staging.ihsgrad.org/application");
} else {
	define("__APPDIR__", "/srv/www/ihsgrad.org/application");
}

//Load initialization code.
require(constant("__APPDIR__")."/initialization.php");

?>