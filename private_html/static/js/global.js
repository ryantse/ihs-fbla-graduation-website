/**
 * Global Tracking Controller
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

var pkBaseURL = "https://analytics.ryantse.me/";
try {
	var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 9);
	piwikTracker.trackPageView();
	piwikTracker.enableLinkTracking();
} catch( err ) {}