var qr_canvas = null,
	qr_reader = null,
	hide_timer = null,
	last_search = "";

jQuery.noConflict();

jQuery(document).ready(function() {
	navigator.getUserMedia || (navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);

	qr_canvas = jQuery("<canvas></canvas>");
	qrcode.callback = processInformation;

	jQuery("#camera_feed").css("width", "100%").css("height", "auto").bind('contextmenu', function() { return false; });

	if(navigator.getUserMedia) {
		try{
			navigator.getUserMedia({audio:false, video:true}, processMedia, failedMedia);
		} catch(e){
			try{
				navigator.getUserMedia('video', processMedia, failedMedia);
			} catch (e){
				failedMedia();
			}
		}

		qr_reader = setInterval(updateCanvas, 500);
	} else {
		failedMedia();
	}
});

function processMedia(stream) {
	window.URL || (window.URL = window.webkitURL);
	var camera_feed = jQuery("#camera_feed").get(0);
	camera_feed.src = URL ? URL.createObjectURL(stream) : stream;
}

function updateCanvas() {
	var camera_feed = jQuery("#camera_feed").get(0);
	qr_canvas.prop("width", camera_feed.videoWidth);
	qr_canvas.prop("height", camera_feed.videoHeight);

	var canvas = qr_canvas.get(0).getContext("2d");
	canvas.clearRect(0, 0, camera_feed.videoWidth, camera_feed.videoHeight);
	canvas.drawImage(camera_feed, 0, 0, camera_feed.videoWidth, camera_feed.videoHeight);
	try {
		qrcode.decode();
	} catch (e) { }
}

function failedMedia() {
	jQuery('<div id="myModal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;"><div class="modal-header"><h3 id="myModalLabel">Initialization Failure</h3></div><div class="modal-body">We were unable to properly initialize the QR code reader.</div><div class="modal-footer"><a class="btn btn-danger" data-dismiss="modal">Close</a></div></div>').modal();
}

function processInformation(search) {
	if(last_search !== search) {
		jQuery("#ticketinfo").html("<h3><div class='alert alert-info'>Searching...</div></h3>");
		jQuery.getJSON("/verification/data/?query="+search, function(data) {
			var html_data = "";
			if(data.status=="STATUS_OK") {
				html_data = html_data + "<h4>Viewing Ticket: Section "+data.section+", Seat Number "+data.seatnumber+"</h4>";
				html_data = html_data + "<h3><div class='alert alert-success'>Valid Ticket</div></h3>";
				html_data = html_data + "<b>Ticket Information:</b><br />";
				html_data = html_data + "Purchased At: "+data.time+"<br />";
				html_data = html_data + "Confirmation Code: "+data.confirmationcode+"<br />";
				html_data = html_data + "Ticket Type: "+data.tickettype+"<br />";
				html_data = html_data + "<br /><b>Owner Information:</b><br />";
				html_data = html_data + "Address: "+data.address1+", "+data.city+", "+data.state+" "+data.zipcode+"<br />";
				html_data = html_data + "Email: "+data.email+"<br />";
			} else {
				html_data = html_data + "<h3><div class='alert alert-error'>Invalid/Unknown Ticket</div></h3>Ticket Data Read: "+search+"<br />";
			}
			jQuery("#ticketinfo").html(html_data);
			try { clearTimeout(hide_timer); } catch (e) {}
			hide_timer = setTimeout(reset, 10000);
		});
		last_search = search;
	}
}

function reset() {
	try { clearTimeout(hide_timer); } catch (e) {}
	last_search = "";
	jQuery("#ticketinfo").html("<h3><div class='alert alert-info'>Waiting for Ticket...</div></h3>");
}