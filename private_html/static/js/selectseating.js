/**
 * Select Seating Controller
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

var current_section = false;
var selected_seats;

jQuery.noConflict();
jQuery(document).ready(function() {
	jQuery.ajaxSetup({"async": false});
	updateselectedseats();

	jQuery(window).resize(function() {
		if(current_section == false) {
			load_sectionselection();
		} else {
			load_seatingselection(current_section);
		}
	});

	jQuery("#select_seating_back").fastClick(function() {
		current_section = false;
		jQuery(window).resize();
	});

	jQuery("#continue").click(function() {
		location.href="/participate/registrationinfo/";
	});

	jQuery(window).resize();
});

function load_sectionselection() {
	jQuery("#selection_anchor").css("width", "100%");
	seating_width = jQuery("#selection_anchor").width();
	seating_height = jQuery("#selection_anchor").width();

	var select_section_area = jQuery("<div></div>");
	select_section_area.css("width", seating_width);
	select_section_area.css("height", seating_height);

	jQuery.getJSON("/participate/seating/full/data/"+seating_width+"/"+seating_height, function(data) {
		select_section_area.empty();
		jQuery.each(data, function(section_id, section_data) {
			var new_section = jQuery("<div class='section'></div>");
			new_section.prop("id", "section_"+section_id);
			new_section.css("width", Math.abs(section_data[0]-section_data[2]));
			new_section.css("height", Math.abs(section_data[1]-section_data[3]));
			new_section.css("left", section_data[0]);
			new_section.css("top", section_data[1]);
			new_section.html("<span>"+section_data[4]+"</span>");
			switch(section_data[5]) {
				case 0:
					new_section.addClass("full");
					break;

				case 1:
					new_section.addClass("reserved");
					break;
			}
			if(section_id.length == 1) {
				new_section.addClass("selectable");
				new_section.fastClick(function() {
					current_section = section_id;
					jQuery(window).resize();
				});
			}
			new_section.disableSelection();
			new_section.appendTo(select_section_area);
		});
		
		jQuery("#select_seating_back").hide();
		jQuery("#select_seating_section").hide();
		jQuery("#first_instruction").show();
		jQuery("#second_instruction").hide();
		jQuery("#select_section").remove();
		jQuery("#select_seating").remove();
		select_section_area.prop("id", "select_section");
		jQuery("#selection_anchor").after(select_section_area);
		normalize_section_fonts();
	});
}

function load_seatingselection(section_name) {
	jQuery("#selection_anchor").css("width", "100%");
	seating_width = jQuery("#selection_anchor").width();
	seating_height = jQuery("#selection_anchor").width();

	var select_seating_area = jQuery("<div></div>");
	select_seating_area.css("width", seating_width);
	select_seating_area.css("height", seating_height);

	jQuery.getJSON("/participate/seating/section/data/"+section_name.toUpperCase()+"/"+seating_width+"/"+seating_height, function(data) {
		select_seating_area.empty();
		jQuery("#select_seating_section").text("Now Viewing: Section "+section_name.toUpperCase());
		jQuery.each(data, function(seat_id, seat_data) {
			var new_seat = jQuery("<div class='seat'></div>");
			new_seat.prop("id", "seat_"+seat_id);
			new_seat.css("width", Math.abs(seat_data[0]-seat_data[2]));
			new_seat.css("height", Math.abs(seat_data[1]-seat_data[3]));
			new_seat.css("left", seat_data[0]);
			new_seat.css("top", seat_data[1]);
			new_seat.data("seat_number", seat_id);
			new_seat.html("<span>"+seat_data[4]+"</span>");
			switch(seat_data[5]) {
				case 0:
					new_seat.addClass("available").addClass("selectable");
					new_seat.fastClick(function() {
						jQuery.getJSON("/participate/seating/reserve/"+section_name+"/"+seat_id, function(return_data) {
							if(return_data.status == "STATUS_NOTOK") {
								jQuery('<div id="myModal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;"><div class="modal-header"><h3 id="myModalLabel">Oops, something went wrong.</h3></div><div class="modal-body">'+return_data.error_message+'</div><div class="modal-footer"><a class="btn btn-danger" data-dismiss="modal">Close</a></div></div>').modal();
							}

							if(return_data.should_refresh_view) {
								jQuery(window).resize();
							}

							if(return_data.should_refresh_view == false && return_data.should_refresh_selection) {
								updateselectedseats();
							}
						});
					});
					break;

				case 1:
					new_seat.addClass("reserved");
					break;
			}
			new_seat.disableSelection();
			new_seat.appendTo(select_seating_area);
		});
		jQuery("#select_seating_back").show();
		jQuery("#select_seating_section").show();
		jQuery("#first_instruction").hide();
		jQuery("#second_instruction").show();
		jQuery("#select_section").remove();
		jQuery("#select_seating").remove();
		select_seating_area.prop("id", "select_seating");
		jQuery("#selection_anchor").after(select_seating_area);
		normalize_seat_fonts();
		updateselectedseats();
	});
}

function normalize_seat_fonts() {
	var smallest_font = 0;
	var original_font_size = 14;
	var original_line_height = "20px";
	jQuery(".seat span").css("font-size", original_font_size).each(function() {
		var new_fontsize = ((jQuery(this).parent().width())/jQuery(this).width()) * original_font_size;
		if(smallest_font > new_fontsize || smallest_font==0) {
			smallest_font = new_fontsize;
		}
	}).promise().done(function() {
		jQuery(".seat span").css({
			"font-size": (smallest_font-2)
		});
		jQuery(".seat span").each(function() {
			var height = jQuery(this).height();
			var offset = (jQuery(this).parent().height()/2)-(height/2);
			jQuery(this).css("position", "relative");
			jQuery(this).css("top", offset);
		});
	});
}

function normalize_section_fonts(select_section_area) {
	var smallest_font = 0;
	var original_font_size = 14;
	var original_line_height = "20px";
	jQuery(".section span").css("font-size", original_font_size).each(function() {
		var new_fontsize = ((jQuery(this).parent().width())/jQuery(this).width()) * original_font_size;
		if(smallest_font > new_fontsize || smallest_font==0) {
			smallest_font = new_fontsize;
		}
	}).promise().done(function() {
		jQuery(".section span").css({
			"font-size": (smallest_font-2)
		});
		jQuery(".section span").each(function() {
			var height = jQuery(this).height();
			var offset = (jQuery(this).parent().height()/2)-(height/2);
			jQuery(this).css("position", "relative");
			jQuery(this).css("top", offset);
		});
	});
}

function updateselectedseats() {
	jQuery.getJSON("/participate/seating/reserve/all", function(data) {
		var total_seats = 0;
		jQuery("#selected_seats tbody").children().remove();
		jQuery.each(data, function(section, section_seats) {
			for(var i=0; i < section_seats.length; i++) {
				jQuery("#selected_seats tbody").append("<tr><td>"+section+"</td><td>"+section_seats[i]+"</td></tr>");
				total_seats++;
			}
		});
		selected_seats = data;
		jQuery("#total_selected_seats").text("Selected Seats: "+total_seats);
		if(total_seats == 0) {
			jQuery("#continue").prop("disabled", true);
		} else {
			jQuery("#continue").prop("disabled", false);
		}
		showselectedseats();
	});
}

function showselectedseats() {
	if(!current_section) { return; }
	if(selected_seats[current_section].length > 0) {
		section_seats = selected_seats[current_section];
		for(var i=0; i<section_seats.length; i++) {
			var seat = jQuery("#seat_"+section_seats[i]);
			seat.addClass("selectable").addClass("selected");
			seat.unbind("click");
			seat.fastClick(function(event) {
				jQuery(this).unbind(event);
				jQuery.getJSON("/participate/seating/unreserve/"+current_section+"/"+jQuery(this).data("seat_number"), function(return_data) {
					if(return_data.status == "STATUS_NOTOK") {
						jQuery('<div id="myModal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;"><div class="modal-header"><h3 id="myModalLabel">Oops, something went wrong.</h3></div><div class="modal-body">'+return_data.error_message+'</div><div class="modal-footer"><a class="btn btn-danger" data-dismiss="modal">Close</a></div></div>').modal();
					}

					if(return_data.should_refresh_view) {
						jQuery(window).resize();
					}

					if(return_data.should_refresh_view == false && return_data.should_refresh_selection) {
						updateselectedseats();
					}
				});
			});
		}
	}
}