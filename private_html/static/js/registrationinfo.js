/**
 * Registration Information Controller
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

jQuery.noConflict();
jQuery(document).ready(function() {
	jQuery("#stripe_creditcard").formatCardNumber();
	jQuery("#stripe_cvc").formatCardCVC();
	jQuery("#stripe_expire").formatCardExpiry();

	jQuery.each(error_data, function(key, value) {
		switch(key) {
			case "payment_authenticator":
				jQuery("#creditcard-error").replaceWith("<div id='creditcard-error' class='alert alert-error'>"+value+"</div>");
				jQuery("#creditcard-error").parent().children("div.control-group").addClass("error");
				break;

			default:
				jQuery("#"+key).parents("div.control-group").addClass("error").attr("data-original-title", value).attr("rel", "tooltip").tooltip();
				jQuery("#"+key).focus(function() {
					jQuery("#"+key).tooltip("show");
				});
				break;
		}
	});

	jQuery("input[name='tos_agreement']").change(function() {
		if(jQuery("input[name='tos_agreement']").is(":checked")) {
			jQuery("#continue").prop("disabled", false);
		} else {
			jQuery("#continue").prop("disabled", true);
		}
	});

	jQuery("input[name='tos_agreement']").change();

	jQuery("#continue").click(function(event) {
		if(load_payment) {
			jQuery("#continue").prop("disabled", true);
			Stripe.setPublishableKey(payment_publickey);
			Stripe.createToken({
				number: jQuery("#stripe_creditcard").val(),
				cvc: jQuery("#stripe_cvc").val(),
				exp_month: jQuery("#stripe_expire").cardExpiryVal().month,
				exp_year: jQuery("#stripe_expire").cardExpiryVal().year
			}, stripeResponseHandler);
			return false;
		} else {
			jQuery("#registrationform").append("<input type='hidden' name='authd' value='1' />");
			jQuery("#registrationform").get(0).submit();
			return false;
		}
	});
});

function stripeResponseHandler(status, response) {
	jQuery("#stripe_creditcard").val("");
	jQuery("#stripe_cvc").val("");
	jQuery("#stripe_expire").val("");
	jQuery("#registrationform").append("<input type='hidden' name='payment_authenticator' value='"+response['id']+"' />");
	jQuery("#registrationform").append("<input type='hidden' name='authd' value='1' />");
	jQuery("#registrationform").get(0).submit();
}