{*

/**
 * Registration Information Template
 *
 * Copyright (c) 2013 Krish Masand, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="/static/css/registrationinfo.css" />
		<script src="https://js.stripe.com/v1/"></script>
		<script type="text/javascript">
			{if isset($error_data)}	var error_data = {$error_data};	{else} var error_data = ""; {/if}
			{if $show_cc_form eq true} var load_payment = true; var payment_publickey = "{$payment_apikey}"; {else} var load_payment = false; {/if}
		</script>
{include file="head.tpl"}
		<script src="/static/js/jquery.payment.js"></script>
		<script src="/static/js/registrationinfo.js"></script>
		<title>Registration Information - IHS '13 Graduation</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="brand">Irvington <img src="/static/img/logo.png"></img> HS</div>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Registration Information</h1>
			<p>Complete the form below to complete your seat reservations. You will have a chance to review your order before it is submitted.</p>
			<div class="row">
				<form class="form-horizontal" method="POST" id="registrationform">
					<div class="span6">
						<h3>Customer Information</h3>
						<div class="control-group">
							<label class="control-label" for="registration_firstname">First Name</label>
							<div class="controls">
								<input type="text" id="registration_firstname" name="registration_firstname" placeholder="" {form_registration_text_restore field="registration_firstname"}>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_middlename">Middle Name</label>
							<div class="controls">
								<input type="text" id="registration_middlename" name="registration_middlename" placeholder="Optional" {form_registration_text_restore field="registration_middlename"}>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_lastname">Last Name</label>
							<div class="controls">
								<input type="text" id="registration_lastname" name="registration_lastname" placeholder="" {form_registration_text_restore field="registration_lastname"}>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_addressline1">Address 1</label>
							<div class="controls">
								<input type="text" id="registration_addressline1" name="registration_addressline1" placeholder="" {form_registration_text_restore field="registration_addressline1"}>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_addressline2">Address 2</label>
							<div class="controls">
								<input type="text" id="registration_addressline2" name="registration_addressline2" placeholder="Optional" {form_registration_text_restore field="registration_addressline2"}>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_city">City</label>
							<div class="controls">
								<input type="text" id="registration_city" name="registration_city" placeholder="" {form_registration_text_restore field="registration_city"}>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_state">State</label>
							<div class="controls">
								<select id="registration_state" name="registration_state">
									<option value="Select a State">Select a State</option>
									<option value="AL" {form_registration_select_restore field="registration_state" value="AL"} >Alabama </option>
									<option value="AK" {form_registration_select_restore field="registration_state" value="AK"} >Alaska </option>
									<option value="AZ" {form_registration_select_restore field="registration_state" value="AZ"} >Arizona </option>
									<option value="AR" {form_registration_select_restore field="registration_state" value="AR"} >Arkansas </option>
									<option value="CA" {form_registration_select_restore field="registration_state" value="CA"} >California </option>
									<option value="CO" {form_registration_select_restore field="registration_state" value="CO"} >Colorado </option>
									<option value="CT" {form_registration_select_restore field="registration_state" value="CT"} >Connecticut </option>
									<option value="DE" {form_registration_select_restore field="registration_state" value="DE"} >Delaware </option>
									<option value="FL" {form_registration_select_restore field="registration_state" value="FL"} >Florida </option>
									<option value="GA" {form_registration_select_restore field="registration_state" value="GA"} >Georgia </option>
									<option value="HI" {form_registration_select_restore field="registration_state" value="HI"} >Hawaii </option>
									<option value="ID" {form_registration_select_restore field="registration_state" value="ID"} >Idaho </option>
									<option value="IL" {form_registration_select_restore field="registration_state" value="IL"} >Illinois </option>
									<option value="IN" {form_registration_select_restore field="registration_state" value="IN"} >Indiana </option>
									<option value="IA" {form_registration_select_restore field="registration_state" value="IA"} >Iowa </option>
									<option value="KS" {form_registration_select_restore field="registration_state" value="KS"} >Kansas </option>
									<option value="KY" {form_registration_select_restore field="registration_state" value="KY"} >Kentucky </option>
									<option value="LA" {form_registration_select_restore field="registration_state" value="LA"} >Louisiana </option>
									<option value="ME" {form_registration_select_restore field="registration_state" value="ME"} >Maine </option>
									<option value="MD" {form_registration_select_restore field="registration_state" value="MD"} >Maryland </option>
									<option value="MA" {form_registration_select_restore field="registration_state" value="MA"} >Massachusetts </option>
									<option value="MI" {form_registration_select_restore field="registration_state" value="MI"} >Michigan </option>
									<option value="MN" {form_registration_select_restore field="registration_state" value="MN"} >Minnesota </option>
									<option value="MS" {form_registration_select_restore field="registration_state" value="MS"} >Mississippi </option>
									<option value="MO" {form_registration_select_restore field="registration_state" value="MO"} >Missouri </option>
									<option value="MT" {form_registration_select_restore field="registration_state" value="MT"} >Montana </option>
									<option value="NE" {form_registration_select_restore field="registration_state" value="NE"} >Nebraska </option>
									<option value="NV" {form_registration_select_restore field="registration_state" value="NV"} >Nevada </option>
									<option value="NH" {form_registration_select_restore field="registration_state" value="NH"} >New Hampshire </option>
									<option value="NJ" {form_registration_select_restore field="registration_state" value="NJ"} >New Jersey </option>
									<option value="NM" {form_registration_select_restore field="registration_state" value="NM"} >New Mexico </option>
									<option value="NY" {form_registration_select_restore field="registration_state" value="NY"} >New York </option>
									<option value="NC" {form_registration_select_restore field="registration_state" value="NC"} >North Carolina </option>
									<option value="ND" {form_registration_select_restore field="registration_state" value="ND"} >North Dakota </option>
									<option value="OH" {form_registration_select_restore field="registration_state" value="OH"} >Ohio </option>
									<option value="OK" {form_registration_select_restore field="registration_state" value="OK"} >Oklahoma </option>
									<option value="OR" {form_registration_select_restore field="registration_state" value="OR"} >Oregon </option>
									<option value="PA" {form_registration_select_restore field="registration_state" value="PA"} >Pennsylvania </option>
									<option value="RI" {form_registration_select_restore field="registration_state" value="RI"} >Rhode Island </option>
									<option value="SC" {form_registration_select_restore field="registration_state" value="SC"} >South Carolina </option>
									<option value="SD" {form_registration_select_restore field="registration_state" value="SD"} >South Dakota </option>
									<option value="TN" {form_registration_select_restore field="registration_state" value="TN"} >Tennessee </option>
									<option value="TX" {form_registration_select_restore field="registration_state" value="TX"} >Texas </option>
									<option value="UT" {form_registration_select_restore field="registration_state" value="UT"} >Utah </option>
									<option value="VT" {form_registration_select_restore field="registration_state" value="VT"} >Vermont </option>
									<option value="VA" {form_registration_select_restore field="registration_state" value="VA"} >Virginia </option>
									<option value="WA" {form_registration_select_restore field="registration_state" value="WA"} >Washington </option>
									<option value="WV" {form_registration_select_restore field="registration_state" value="WV"} >West Virginia </option>
									<option value="WI" {form_registration_select_restore field="registration_state" value="WI"} >Wisconsin </option>
									<option value="WY" {form_registration_select_restore field="registration_state" value="WY"} >Wyoming </option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_zipcode">Zip Code</label>
							<div class="controls">
								<input type="text" id="registration_zipcode" name="registration_zipcode" placeholder="" {form_registration_text_restore field="registration_zipcode"}>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="registration_email">Email</label>
							<div class="controls">
								<input type="email" id="registration_email" name="registration_email" placeholder="" {form_registration_text_restore field="registration_email"}>
							</div>
						</div>
					</div>
					<div class="span6">
						<h3>Credit Card Information</h3>
						{if $show_cc_form}
							<a id="creditcard-error"></a>
							<div class="control-group">
								<label class="control-label" for="billing_creditcard">Credit Card #</label>
								<div class="controls">
									<input type="text" id="stripe_creditcard" name="stripe_creditcard" placeholder="&#9679;&#9679;&#9679;&#9679; &#9679;&#9679;&#9679;&#9679; &#9679;&#9679;&#9679;&#9679; &#9679;&#9679;&#9679;&#9679;">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="billing_cvc">CVC Code</label>
								<div class="controls">
									<input type="text" id="stripe_cvc" name="stripe_cvc" placeholder="">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="stripe_expire">Expiration Date</label>
								<div class="controls">
									<input type="text" id="stripe_expire" name="stripe_expire" placeholder="MM / YY">
								</div>
							</div>
						{else}
							<p>Your credit card information has been saved and will be discarded after completing this secure transaction.</p>
							<a class="btn" href="/participate/registrationinfo/billingreset">Clear Billing Information</a>
						{/if}
						<h3>Terms of Service</h3>
						<label id="tos_agreement" class="checkbox">
							<input name="tos_agreement" type="checkbox" value="1" {form_registration_checkbox_restore field="registration_agreetos"} > I have read and agree to the <a target="_blank" href="/participate/terms">terms of service</a> and <a target="_blank" href="/participate/privacy">privacy policy</a>.
						</label>
						<button id="continue" class="btn btn-primary">Continue</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>