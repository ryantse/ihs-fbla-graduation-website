{*

/**
 * Seat Selection Template
 *
 * Copyright (c) 2013 Krish Masand, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="/static/css/selectseating.css" />
{include file="head.tpl"}
		<title>Select Seating - IHS '13 Graduation</title>
		<script src="/static/js/jquery.disableselection.js"></script>
		<script src="/static/js/jquery.fastclick.js"></script>
		<script src="/static/js/selectseating.js"></script>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="brand">Irvington <img src="/static/img/logo.png"></img> HS</div>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Select Seating</h1>
			<div class="row">
				<div class="span6">
					<p id="first_instruction">Please begin by selecting a seating section.</p>
					<p id="second_instruction">Select which seats you would like. To go back and select another section, hit "back to section selection".</p>
					<noscript>
						<p>Oops, it seems we have run into a problem.<br /></p>
						<div class="alert alert-error">Javascript is required for the registration process. Please enable Javascript or switch to a compatible browser to continue.</div>
					</noscript>
					<button id="select_seating_back" class="btn">Back to Section Selection</button>
					<h4 id="select_seating_section"></h4>
					<div id="selection_anchor"></div>
				</div>
				<div class="span6">
					<h3>Selected Seats</h3>
					<div class="alert alert-info">Seats are reserved for up to 10 minutes after selection. The seats you select here are <b><u>not</u></b> guaranteed until after you have confirmed your payment.</div>
					<h4 id="total_selected_seats"></h4>
					<table id="selected_seats" class="table table-bordered table-striped table-hover">
						<thead>
							<th>Seat Section</th>
							<th>Seat Number</th>
						</thead>
						<tbody>
						</tbody>
					</table>
					<div class="pull-right">
						<button id="continue" class="btn btn-primary">Continue</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>