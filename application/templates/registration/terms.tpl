{*

/**
 * Terms of Service
 *
 * Copyright (c) 2013 Krish Masand, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
{include file="head.tpl"}
		<title>Terms of Service - IHS '13 Graduation</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="/">Irvington <img src="/static/img/logo.png"></img> HS</a>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Terms of Service</h1>
			<p><strong>Agreement  between user and </strong><strong>Irvington High School</strong> <br/>
			Welcome to https://ihsgrad.org. The https://ihsgrad.org website (the &quot;Site&quot;)  is comprised of various web pages operated by Irvington High School  (&quot;IHS&quot;). https://ihsgrad.org  is offered to you conditioned on your acceptance without  modification of the terms, conditions, and notices contained herein (the  &quot;Terms&quot;). Your use of https://ihsgrad.org  constitutes your agreement to all such Terms. Please read these  terms carefully, and keep a copy of them for your reference. </p>
			<p>https://ihsgrad.org  is a E-commerce Site <br/>
			The purpose of https://ihsgrad.org/ is to enable students and  their families to reserve and/or purchase seats to attend the Class of 2013's  graduation ceremony, which will be on 6/20 at Tak Fudenna Stadium in Fremont,  CA. </p>
			<p><strong>Privacy</strong> <br/>
			Your use of https://ihsgrad.org is subject to IHS's Privacy  Policy. Please review our <a href="/participate/privacy">Privacy Policy</a>, which also governs the Site and  informs users of our data collection practices. </p>
			<p><strong>Electronic  Communications</strong> <br/>
			Visiting https://ihsgrad.org or sending emails to IHS  constitutes electronic communications. You consent to receive electronic  communications and you agree that all agreements, notices, disclosures and  other communications that we provide to you electronically, via email and on  the Site, satisfy any legal requirement that such communications be in writing. <br/>
			 <br/>
			IHS does not knowingly collect, either online  or offline, personal information from persons under the age of thirteen. If you  are under 18, you may use https://ihsgrad.org  only with permission of a parent or guardian. <br/>
			 <br/>
			<strong>Links  to third party sites/Third party services</strong> <br/>
			https://ihsgrad.org  irvington may contain links to other websites (&quot;Linked  Sites&quot;). The Linked Sites are not under the control of IHS and IHS is not  responsible for the contents of any Linked Site, including without limitation  any link contained in a Linked Site, or any changes or updates to a Linked  Site. IHS is providing these links to you only as a convenience, and the  inclusion of any link does not imply endorsement by IHS of the site or any  association with its operators. <br/>
			 <br/>
			Certain services made available via https://ihsgrad.org are  delivered by third party sites and organizations. By using any product, service  or functionality originating from the https://ihsgrad.org, you hereby acknowledge and  consent that IHS may share such information and data with any third party with  whom IHS has a contractual relationship to provide the requested product,  service or functionality on behalf of https://ihsgrad.org users and customers. <br/>
			 <br/>
			<strong>No  unlawful or prohibited use/Intellectual Property </strong> <br/>
			You are granted a non-exclusive,  non-transferable, revocable license to access and use https://ihsgrad.org strictly  in accordance with these terms of use. As a condition of your use of the Site,  you warrant to IHS that you will not use the Site for any purpose that is  unlawful or prohibited by these Terms. You may not use the Site in any manner  which could damage, disable, overburden, or impair the Site or interfere with  any other party's use and enjoyment of the Site. You may not obtain or attempt  to obtain any materials or information through any means not intentionally made  available or provided for through the Site. <br/>
			 <br/>
			All content included as part of the Service,  such as text, graphics, logos, images, as well as the compilation thereof, and  any software used on the Site, is the property of IHS or its suppliers and protected  by copyright and other laws that protect intellectual property and proprietary  rights. You agree to observe and abide by all copyright and other proprietary  notices, legends or other restrictions contained in any such content and will  not make any changes thereto. <br/>
			 <br/>
			You will not modify, publish, transmit,  reverse engineer, participate in the transfer or sale, create derivative works,  or in any way exploit any of the content, in whole or in part, found on the  Site. IHS content is not for resale. Your use of the Site does not entitle you  to make any unauthorized use of any protected content, and in particular you  will not delete or alter any proprietary rights or attribution notices in any  content. You will use protected content solely for your personal use, and will  make no other use of the content without the express written permission of IHS  and the copyright owner. You agree that you do not acquire any ownership rights  in any protected content. We do not grant you any licenses, express or implied,  to the intellectual property of IHS or our licensors except as expressly  authorized by these Terms. <br/>
			 <br/>
			<strong>International  Users</strong> <br/>
			The Service is controlled, operated and  administered by IHS from our offices within the USA. If you access the Service  from a location outside the USA, you are responsible for compliance with all  local laws. You agree that you will not use the IHS Content accessed through https://ihsgrad.org in any  country or in any manner prohibited by any applicable laws, restrictions or  regulations. <br/>
			 <br/>
			<strong>Indemnification</strong> <br/>
			You agree to indemnify, defend and hold  harmless IHS, its officers, directors, employees, agents and third parties, for  any losses, costs, liabilities and expenses (including reasonable attorneys'  fees) relating to or arising out of your use of or inability to use the Site or  services, any user postings made by you, your violation of any terms of this  Agreement or your violation of any rights of a third party, or your violation  of any applicable laws, rules or regulations. IHS reserves the right, at its  own cost, to assume the exclusive defense and control of any matter otherwise  subject to indemnification by you, in which event you will fully cooperate with  IHS in asserting any available defenses. <br/>
			 <br/>
			<strong>Liability  disclaimer</strong> <br/>
			THE INFORMATION, SOFTWARE, PRODUCTS, AND  SERVICES INCLUDED IN OR AVAILABLE THROUGH THE SITE MAY INCLUDE INACCURACIES OR  TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN.  IRVINGTON HIGH SCHOOL AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES  IN THE SITE AT ANY TIME. <br/>
			 <br/>
			IRVINGTON HIGH SCHOOL AND/OR ITS SUPPLIERS  MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY,  TIMELINESS, AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND  RELATED GRAPHICS CONTAINED ON THE SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT  PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES  AND RELATED GRAPHICS ARE PROVIDED &quot;AS IS&quot; WITHOUT WARRANTY OR  CONDITION OF ANY KIND. IRVINGTON HIGH SCHOOL AND/OR ITS SUPPLIERS HEREBY  DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION,  SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED  WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,  TITLE AND NON-INFRINGEMENT. <br/>
			 <br/>
			TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE  LAW, IN NO EVENT SHALL IRVINGTON HIGH SCHOOL AND/OR ITS SUPPLIERS BE LIABLE FOR  ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR  ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE,  DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR  PERFORMANCE OF THE SITE, WITH THE DELAY OR INABILITY TO USE THE SITE OR RELATED  SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY  INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH  THE SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON  CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF IRVINGTON  HIGH SCHOOL OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF  DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR  LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE  LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF  THE SITE, OR WITH ANY OF THESE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO  DISCONTINUE USING THE SITE. <br/>
			 <br/>
			<strong>Termination/access  restriction </strong> <br/>
			IHS reserves the right, in its sole  discretion, to terminate your access to the Site and the related services or  any portion thereof at any time, without notice. To the maximum extent permitted  by law, this agreement is governed by the laws of the State of California and  you hereby consent to the exclusive jurisdiction and venue of courts in  California in all disputes arising out of or relating to the use of the Site.  Use of the Site is unauthorized in any jurisdiction that does not give effect  to all provisions of these Terms, including, without limitation, this section. <br/>
			 <br/>
			You agree that no joint venture, partnership,  employment, or agency relationship exists between you and IHS as a result of  this agreement or use of the Site. IHS's performance of this agreement is  subject to existing laws and legal process, and nothing contained in this  agreement is in derogation of IHS's right to comply with governmental, court  and law enforcement requests or requirements relating to your use of the Site  or information provided to or gathered by IHS with respect to such use. If any  part of this agreement is determined to be invalid or unenforceable pursuant to  applicable law including, but not limited to, the warranty disclaimers and  liability limitations set forth above, then the invalid or unenforceable  provision will be deemed superseded by a valid, enforceable provision that most  closely matches the intent of the original provision and the remainder of the  agreement shall continue in effect. <br/>
			 <br/>
			Unless otherwise specified herein, this  agreement constitutes the entire agreement between the user and IHS with  respect to the Site and it supersedes all prior or contemporaneous  communications and proposals, whether electronic, oral or written, between the  user and IHS with respect to the Site. A printed version of this agreement and  of any notice given in electronic form shall be admissible in judicial or  administrative proceedings based upon or relating to this agreement to the same  extent an d subject to the same conditions as other business documents and  records originally generated and maintained in printed form. It is the express  wish to the parties that this agreement and all related documents be written in  English. <br/>
			 <br/>
			<strong>Changes  to Terms</strong> <br/>
			IHS reserves the right, in its sole  discretion, to change the Terms under which https://ihsgrad.org is offered. The most current  version of the Terms will supersede all previous versions. IHS encourages you  to periodically review the Terms to stay informed of our updates. <br/>
			<br/>
			<strong>Contact  Us</strong> <br/>
			IHS welcomes your questions or comments  regarding the Terms: <br/>
			 <br/>
			Irvington High School <br/>
			41800 Blacow Road, Fremont<br/>
			Fremont, CA 94538 <br/>
			 <br/>
			 Email Address: <br/>
			support@ihsgrad.org<br/>
			 <br/>
			Telephone Number: <br/>
			(510) 656-5711 <br/>
			 <br/>
			Effective as of February 08, 2013 </p>
		</div>
	</body>
</html>