{*

/**
 * Registration Information Verification
 *
 * Copyright (c) 2013 Krish Masand, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
{include file="head.tpl"}
		<title>Verify Information - IHS '13 Graduation</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="brand">Irvington <img src="/static/img/logo.png"></img> HS</div>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Verify Information</h1>
			<p>Verify the information below is correct and click "Submit" to complete your purchase.</p>
			<div class="row">
				<div class="span6">
					<h3>Customer Information:</h3>
					{$registration_info.registration_firstname} {$registration_info.registration_middlename} {$registration_info.registration_lastname}<br />
					{$registration_info.registration_addressline1}<br />
					{if $registration_info.registration_addressline2 eq ''}
					{else}
						{$registration_info.registration_addressline2}<br />
					{/if}
					{$registration_info.registration_city}, {$registration_info.registration_state} {$registration_info.registration_zipcode}<br />
					{$registration_info.registration_email}<br />
					<br />
				</div>

				<div class="span6">
					<h3>Billing Information:</h3>
					Card Type: {$payment_info.type}<br />
					Credit Card Number: &#9679;&#9679;&#9679;&#9679;&nbsp;&#9679;&#9679;&#9679;&#9679;&nbsp;&#9679;&#9679;&#9679;&#9679;&nbsp;{$payment_info.last4}<br />
					Expiration: {$payment_info.exp_month}/{$payment_info.exp_year}<br />
				</div>
			</div>
			<a class="btn" href="/participate/registrationinfo/">Return to Registration Information</a><br />
			<div class="clear"></div>
			<h3>Checkout Summary</h3>
			{if $has_student_seats}
				<div class="alert alert-info">You have selected one or more student tickets. At the entrance, you will be required to produce a student ID prior to entry.</div>
			{/if}
			{if isset($error_message)}
				<div class="alert alert-error">{$error_message}</div>
			{/if}
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<th>Item Name</th>
					<th>Item Description</th>
					<th>Item Price</th>
				</thead>
				<tbody>
					{foreach from=$checkout_item_list key=k item=item}
					<tr>
						<td>{$item.section}{$item.seat}</td>
						<td>{$item.description}</td>
						<td>${$item.price}</td>
					</tr>
					{/foreach}
					<tr>
						<td colspan="2"><b>Total</b></td>
						<td>${$checkout_total}</td>
					</tr>
				</tbody>
			</table>
			<a class="btn" href="/participate/selectseating/">Return to Seat Selection</a>
			<div class="pull-right">
				<form method="POST">
					<input type="hidden" name="authd" value="1" />
					<button id="submit" type="submit" name="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</body>
</html>