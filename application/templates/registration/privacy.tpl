{*

/**
 * Privacy Policy
 *
 * Copyright (c) 2013 Krish Masand, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
{include file="head.tpl"}
		<title>Privacy Policy - IHS '13 Graduation</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="/">Irvington <img src="/static/img/logo.png"></img> HS</a>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Privacy Policy</h1>
			<p>
				Protecting your private information is our  priority. This Statement of Privacy applies to Irvington High School and  governs data collection and usage. For the purposes of this Privacy Policy,  unless otherwise noted, all references to Irvington High School include IHS, https://ihsgrad.org/.  The purpose of https://ihsgrad.org/ is to enable students and their families to  reserve and/or purchase seats to attend the Class of 2013's graduation  ceremony, which will be on 6/20 at Tak Fudenna Stadium in Fremont, CA. site. By  using the IHS website, you consent to the data practices described in this  statement. <br/>
				<br/>
				<strong>Collection  of your Personal Information</strong> <br/>
				IHS may collect personally identifiable  information, such as your name. If you purchase IHS's products and services, we  collect billing and credit card information. This information is used to  complete the purchase transaction. We may gather additional personal or  non-personal information in the future. <br/>
				<br/>
				Information about your computer hardware and  software may be automatically collected by IHS. This information can include:  your IP address, browser type, domain names, access times and referring website  addresses. This information is used for the operation of the service, to  maintain quality of the service, and to provide general statistics regarding  use of the IHS website. <br/>
				<br/>
				IHS encourages you to review the privacy  statements of websites you choose to link to from IHS so that you can  understand how those websites collect, use and share your information. IHS is  not responsible for the privacy statements or other content on websites outside  of the IHS website. <br/>
				<br/>
				<strong>Use of  your Personal Information </strong> <br/>
				IHS collects and uses your personal  information to operate its website(s) and deliver the services you have  requested. <br/>
				<br/>
				IHS may also use your personally identifiable  information to inform you of other products or services available from IHS and  its affiliates. IHS may also contact you via surveys to conduct research about  your opinion of current services or of potential new services that may be  offered. <br/>
				<br/>
				IHS does not sell, rent or lease its customer  lists to third parties. <br/>
				<br/>
				IHS may share data with trusted partners to  help perform statistical analysis, send you email or postal mail, provide  customer support, or arrange for deliveries. All such third parties are  prohibited from using your personal information except to provide these  services to IHS, and they are required to maintain the confidentiality of your  information. <br/>
				<br/>
				IHS will disclose your personal information,  without notice, only if required to do so by law or in the good faith belief  that such action is necessary to: (a) conform to the edicts of the law or  comply with legal process served on IHS or the site; (b) protect and defend the  rights or property of IHS; and, (c) act under exigent circumstances to protect  the personal safety of users of IHS, or the public. <br/>
				<br/>
				<strong>Security  of your Personal Information</strong> <br/>
				To secure your personal information from  unauthorized access, use or disclosure, IHS uses the following: <br/>
				<br/>
				PCI-1 compliance with Stripe Payment Services <br/>
				<br/>
				When personal information (such as a credit  card number) is transmitted to other websites, it is protected through the use  of encryption, such as the Secure Sockets Layer (SSL) protocol. <br/>
				<br/>
				<strong>Children  Under Thirteen</strong> <br/>
				IHS does not knowingly collect personally  identifiable information from children under the age of thirteen. If you are  under the age of thirteen, you must ask your parent or guardian for permission  to use this website. <br/>
				<br/>
				<strong>Changes  to this Statement</strong> <br/>
				IHS will occasionally update this Statement of  Privacy to reflect company and customer feedback. IHS encourages you to  periodically review this Statement to be informed of how IHS is protecting your  information. <br/>
				<br/>
				<strong>Contact  Information</strong> <br/>
				IHS welcomes your questions or comments  regarding this Statement of Privacy. If you believe that IHS has not adhered to  this Statement, please contact IHS at: <br/>
				<br/>
				Irvington High School <br/>
				41800 Blacow Road <br/>
				Fremont, CA 94538 <br/>
				<br/>
				Email Address: <br/>
				support@ihsgrad.org<br/>
				<br/>
				Telephone Number: <br/>
				(510) 656-5711 <br/>
				<br/>
				Effective as of February 08, 2013 <br/>
			</p>
		</div>
	</body>
</html>