{*

/**
 * Confirmation Page
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
{include file="head.tpl"}
		<title>Confirmation - IHS '13 Graduation</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="/">Irvington <img src="/static/img/logo.png"></img> HS</a>
					<div class="nav-collapse collapse">
						<ul class="nav pull-right">
							<li><a href="/">Return Home</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Confirmation</h1>
			<p>Congratulations, your seats have been reserved! Your reservation code is <b>{$confirmation_id}</b>.</p>
			<p>An email has been sent to you containing the confirmation code. Keep this for your personal records as a proof of purchase. Also, e-tickets have been emailed to you as well. You may either print out the tickets or present the email to the ticketing agent at the entrance. Alternatively, you can use the confirmation code to pickup the tickets at the ticketing counter on the day of the event.</p>
			<h3>Location</h3>
			<b>Date:</b> June 20, 2013 at 10:30 AM<br />
			<br />
			<address>
				<strong>Tak Fudenna Memorial Stadium</strong><br />
				38442 Fremont Boulevard,<br />
				Fremont, CA, 94536<br />
			</address>
			<h4>Get Directions</h4>
			<form action="http://maps.google.com/maps"  method="GET" target="_blank">
				<div class="control-group">
					<label class="control-label" for="saddr">Starting Address: </label>
					<div class="controls">
						<input type="text" name="saddr" class="input-xlarge" placeholder="" />
					</div>
					<label class="control-label">Destination: </label>
					<div class="controls">
						<span class="input-xlarge uneditable-input">TAK Stadium, Fremont, CA</span>
					</div>
				</div>
				<input type="hidden" name="daddr" value="TAK Stadium, Fremont, CA" />
				<input type="submit" value="Get Directions" class="btn btn-primary" />
			</form>
		</div>
	</body>
</html>