{*

/**
 * Index Page Template
 *
 * Copyright (c) 2013 Krish Masand, Mark Tai, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="/static/css/index.css" />
{include file="head.tpl"}
		<title>Irvington High School - Graduation 2013</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="/">Irvington <img src="/static/img/logo.png"></img> HS</a>
					<div class="nav-collapse collapse">
						<ul class="nav pull-right">
							<li><a href="/reservations">Retrieve Tickets</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="hero-unit">
			<div class="container">
				<h1>Senior Graduation 2013</h1>
				<p>Join in on the memories of a lifetime.</p>	
				<p id="participate_button"><a href="/participate/" class="btn btn-primary btn-large">Participate</a></p>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="span6">
					<h3>Event Details</h3>
					<p>Marking the start of their adult lives, Irvington Seniors graduate into their futures. They ascend into maturity and take their first steps outside of their familiar land and start to reach across the world and disperse. Witness this change, share the exuberance, and know the accomplishments of these seniors. </p>
					<p>Throughout the many years, thousands of high school students have stood on the stage as they prepare for their future. An outstanding student, chosen by his or her classmates, is to come up and deliver a speech to send off their class from Irvington and propel them into their new environment. Students also are presented awards acknowledging their respective achievements.  achievements. After the ceremony, the audience may join their respective graduate for a barrage of pictures that will last a lifetime.</p>
				</div>
				<div class="span6">
					<h4>Date:</h4>
					June 20, 2013 at 10:30 AM<br />
					<h4>Address:</h4>
					<address>
						<strong>Tak Fudenna Memorial Stadium</strong><br />
						38442 Fremont Boulevard,<br />
						Fremont, CA, 94536<br />
					</address>
					<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=&amp;aq=0&amp;oq=tak+fud&amp;sll=37.553101,-121.991002&amp;sspn=0.005876,0.008256&amp;t=h&amp;ie=UTF8&amp;ll=37.553243,-121.991394&amp;spn=0.002406,0.00214&amp;z=18&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=&amp;aq=0&amp;oq=tak+fud&amp;sll=37.553101,-121.991002&amp;sspn=0.005876,0.008256&amp;t=h&amp;ie=UTF8&amp;ll=37.553243,-121.991394&amp;spn=0.002406,0.00214&amp;z=18" target="_blank">View Larger Map</a></small>
					<h4>Get Directions</h4>
					<form action="http://maps.google.com/maps"  method="GET" target="_blank">
						<div class="control-group">
							<label class="control-label" for="saddr">Starting Address: </label>
							<div class="controls">
								<input type="text" name="saddr" class="input-xlarge" placeholder="" />
							</div>
							<label class="control-label">Destination: </label>
							<div class="controls">
								<span class="input-xlarge uneditable-input">TAK Stadium, Fremont, CA</span>
							</div>
						</div>
						<input type="hidden" name="daddr" value="TAK Stadium, Fremont, CA" />
						<input type="submit" value="Get Directions" class="btn btn-primary" />
					</form>
				</div>
			</div>
		</div>
		<div class="container">
			<footer class="footer">
				<p class="pull-right">
					<a href= "/participate/terms"> Terms of Service</a> | <a href="/participate/privacy">Privacy Policy</a>
					<span class="visible-desktop hidden-tablet hidden-phone">Scroll image from <a href="http://www.freeimages.co.uk" target="_blank">FreeImages</a>.</span>
				</p>
				<p>
					Copyright &copy; IHS FBLA, written by Ryan Tse, Mark Tai, and Krish Masand.<br />
					Powered by <a href="http://twitter.github.com/bootstrap/" target="_blank">Twitter Bootstrap</a>.
				</p>
			</footer>
		</div>
	</body>
</html>