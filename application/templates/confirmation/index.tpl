{*

/**
 * Reservation Lookup Page
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}

<!DOCTYPE html>
<html lang="en">
	<head>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" />
{include file="head.tpl"}
		<title>Retrieve Tickets - IHS '13 Graduation</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="/">Irvington <img src="/static/img/logo.png"></img> HS</a>
					<div class="nav-collapse collapse">
						<ul class="nav pull-right">
							<li><a href="/">Return Home</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Retrieve Tickets</h1>
			<p>To retrieve your tickets, please enter the email and confirmation code provided to you.</p>
			{if $error_occurred}
				<div class="alert alert-error">We were unable to locate any tickets associated with the email and confirmation ID you supplied.</div>
			{/if}
			<form method="POST" class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="reservation_email">Email</label>
					<div class="controls">
						<input type="text" id="reservation_email" name="reservation_email" placeholder="">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="reservation_confirmation">Confirmation ID</label>
					<div class="controls">
						<input type="text" id="reservation_confirmation" name="reservation_confirmation" placeholder="">
					</div>
				</div>
				<input type="hidden" name="authd" value="1" />
				<div class="control-group">
					<div class="controls">
						<button id="submit" type="submit" name="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>