{*

/**
 * Verification Kiosk Code
 *
 * Copyright (c) 2013 Krish Masand, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

*}
<!DOCTYPE html>
<html lang="en">
	<head>
{include file="head.tpl"}
		<title>Ticket Validation - IHS '13 Graduation</title>
		<script src="/static/js/readqr.js"></script>
		<script src="/static/js/verification.js"></script>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="brand">Irvington <img src="/static/img/logo.png"></img> HS</div>
				</div>
			</div>
		</div>
		<div class="container">
			<h1>Ticket Validation</h1>
			<div class="row">
				<div class="span6">
					<h3>QR Code Reader</h3>
					<video id="camera_feed" autoplay></video>
				</div>
				<div class="span6">
					<h3>Ticket Status</h3>
					<div id="ticketinfo"><h3><div class='alert alert-info'>Waiting for Ticket...</div></h3></div>
				</div>
			</div>
		</div>
	</body>
</html>