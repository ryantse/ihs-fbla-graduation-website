<?php

/**
 * Form selection value restoration.
 *
 * Copyright (c) 2013 Ryan Tse.
 * Last Modified: Febuary 6th, 2013
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

function smarty_function_form_registration_select_restore($params, $smarty) {
	if(isset($_SESSION["verify_registrationinfo"][$params["field"]]) && $_SESSION["verify_registrationinfo"][$params["field"]] == $params["value"]) {
		return "selected=\"selected\"";
	}
}

?>