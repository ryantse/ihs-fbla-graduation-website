<?php

/**
 * Simple input rules checker.
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class InputRules {
	private $fields;
	private $errors;
	private $values;

	public function __construct(&$target) {
		$this->values = &$target;
		$this->fields = array();
		$this->preprocessors = array();
	}

	//Add a rule to the fields required.
	public function add($field, $check, $error_message) {
		if(!isset($this->fields[$field])) {
			$this->fields[$field] = array();
		}
		$properties = array(
			"check" => $check,
			"required" => true,
			"error_message" => $error_message
		);
		array_push($this->fields[$field], $properties);
	}

	public function addoptional($field, $check, $error_message) {
		if(!isset($this->fields[$field])) {
			$this->fields[$field] = array();
		}
		$properties = array(
			"check" => $check,
			"required" => false,
			"error_message" => $error_message
		);
		array_push($this->fields[$field], $properties);
	}

	public function addpreprocessor($field, $processor) {
		if(!isset($this->preprocessors[$field])) {
			$this->preprocessors[$field] = array();
		}
		$properties = array(
			"preprocessor" => $processor
		);
		array_push($this->preprocessors[$field], $properties);
	}

	//Run through all associated checks.
	public function check() {
		foreach ($this->preprocessors as $field => $checks) {
			foreach($checks as $properties) {
				if(isset($this->values[$field])) {
					call_user_func($properties["preprocessor"], &$this->values[$field]);
				}
			}
		}
		$this->errors = array();
		foreach($this->fields as $field => $checks) {
			try {
				foreach($checks as $properties) {
					if($properties["required"] || isset($this->values[$field])) {
						if(call_user_func($properties["check"], $this->values[$field]) == FALSE) {
							throw new InputError($properties["error_message"]);
						}
					}
				}
			} catch (InputError $error) {
				$this->errors[$field] = $error->getMessage();
			}
		}
		return (count($this->errors) == 0);
	}

	public function geterrors() {
		return $this->errors;
	}
}

class InputError extends Exception {}