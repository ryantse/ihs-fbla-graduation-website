<?php

/**
 * Configuration Management Class
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Configuration {
	private $_storageName     = null;
	private $_storageArray    = array();
	
	private static $instances = array();

	public static function open($name) {
		if(array_key_exists($name, self::$instances)) {
			return self::$instances[$name];
		}

		self::$instances[$name] = new self($name);
		return self::$instances[$name];
	}

	public static function exists($name) {
		if(array_key_exists($name, self::$instances)) {
			return true;
		}

		return false;
	}

	public function __construct($name) {
		$this->_storageName = $name;
	}

	public function __destroy() {
		unset(self::$instsances[$this->_storageName]);
	}

	public function __set($name, $value) {
		$this->_storageArray[$name] = $value;
	}

	public function __get($name) {
		if(array_key_exists($name, $this->_storageArray)) {
			return $this->_storageArray[$name];
		}

		return null;
	}

	public function __isset($name) {
		return isset($this->_storageArray[$name]);
	}

	public function __unset($name) {
		unset($this->_storageArray[$name]);
	}
}

?>