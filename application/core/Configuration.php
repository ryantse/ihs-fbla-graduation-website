<?php

/**
 * Server Configuration
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

//Application configuration
$application                 = Configuration::open("APPLICATION");
$application->url            = "https://ihsgrad.org";
$application->ssl            = true;
$application->security_token = "ZGgFhhUmEPV5oDM6yYqs8FE2kzeq1yddOzp6VWWcisUjf94je7HsqJ17kPjFRkA";

//Database configuration
$database                    = Configuration::open("DATABASE");
$database->host              = "localhost";
$database->username          = "ihsgrad_registra";
$database->password          = "eNcQspIS*QD(gr";
$database->database          = "ihsgrad_registrations";

//Payment configuration
$payment                     = Configuration::open("PAYMENT");
$payment->private_apikey     = "sk_test_rXmZnLDCQJtWOn5GKzwB8fBn";
$payment->public_apikey      = "pk_test_oAeeqiKSFKJKamcnlZ1yZDoA";

//Mailer Configuration
$email                       = Configuration::open("EMAIL");
$email->name                 = "IHS Graduation 2013";
$email->email                = "no-reply@ihsgrad.org";

//Testing configuration
if(defined("__TESTING__") && (constant("__TESTING__") == TRUE)) {
	$database->username      = "ihsgrad_staging";
	$database->database      = "ihsgrad_staging";
	$application->url        = "https://staging.ihsgrad.org";
}
?>