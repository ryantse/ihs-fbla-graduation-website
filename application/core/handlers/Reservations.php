<?php

/**
 * /reservations/ URL Handler and Logic Controller
 *
 * Copyright (c) 2013 Ryan Tse, Mark Tai.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Reservations extends Pages {
	public function __construct() {
		//Load both templating and session engines.
		parent::__construct(true, true);

		$database_config = Configuration::open("DATABASE");
		$this->database = MySqlDatabase::getInstance();
		$this->database->connect($database_config->host, $database_config->username, $database_config->password, $database_config->database);
	}

	public function index() {
		$this->template_data->assign("error_occurred", false);
		if($_SERVER["REQUEST_METHOD"]=="POST") {
			$_POST["reservation_confirmation"] = strtoupper($_POST["reservation_confirmation"]);
			$mysql_query = "SELECT first_name, middle_name, last_name, confirmation_id FROM transactions WHERE confirmation_id='".mysql_real_escape_string($_POST["reservation_confirmation"])."' AND email='".mysql_real_escape_string($_POST["reservation_email"])."' LIMIT 1";
			$result = $this->database->fetchOneRow($mysql_query);
			if(isset($result->first_name)) {
				$full_name = $result->first_name;
				if($result->middle_name !== "") {
					$full_name .= " ".$result->middle_name;
				}
				$full_name .= " ".$result->last_name;
				$mysql_query = "SELECT section_id, seat_number, seat_type FROM seats WHERE seat_data='".mysql_real_escape_string($_POST["reservation_confirmation"])."'";
				$seat_results = $this->database->query($mysql_query);
				$_SESSION["valid_till"] = strtotime("+30 seconds");
				$_SESSION["tickets_tickets"] = array();
				while($row = mysql_fetch_assoc($seat_results)) {
					$_SESSION["tickets_tickets"][] = array(
						"ticket_type" => $row["seat_type"],
						"seat_section" => $row["section_id"],
						"seat_number" => $row["seat_number"],
						"confirmation_code" => $_POST["reservation_confirmation"],
						"ticket_owner" => strtoupper($full_name)
					);
				}

				header("Location: /reservations/tickets.pdf");
			} else {
				$this->template_data->assign("error_occurred", true);
			}
		}
		$this->template->display("confirmation/index.tpl", $this->template_data);
	}

	public function tickets() {
		if(isset($_SESSION["tickets_tickets"]) && isset($_SESSION["valid_till"]) && ($_SESSION["valid_till"] > time())) {
			$ticket_data = $_SESSION["tickets_tickets"];
			$ticket_controller = new TicketController();
			$ticket_controller->generate_tickets($ticket_data);
		} else {
			if(isset($_SESSION["valid_till"]) && ($_SESSION["valid_till"] < time())) {
				unset($_SESSION["tickets_tickets"]);
			}
			header("Location: /reservations/");
		}
	}
}