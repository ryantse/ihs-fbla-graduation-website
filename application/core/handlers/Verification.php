<?php

/**
 * Ticket verification for counter
 *
 * Copyright (c) 2013 Ryan Tse, Mark Tai, Krish Masand.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Verification extends Pages {
	public function __construct() {
		parent::__construct(true, false);


		$database_config = Configuration::open("DATABASE");
		$this->database = MySqlDatabase::getInstance();
		$this->database->connect($database_config->host, $database_config->username, $database_config->password, $database_config->database);
	}

	public function index() {
		$this->template->display("verification/index.tpl",  $this->template_data);
	}

	public function lookup() {
		$application = Configuration::open("APPLICATION");

		$lookup_string = $_GET["query"];
		$lookup_data = explode("|", $lookup_string, 3);

		$mysql_query = "SELECT * FROM seats WHERE section_id='".mysql_real_escape_string($lookup_data[0])."' AND seat_number='".mysql_real_escape_string($lookup_data[1])."' LIMIT 1";
		$seat_data = $this->database->fetchOneRow($mysql_query);
		$confirmation_hash = strtoupper(substr(hash("sha256",$application->securitytoken.$lookup_data[1].$lookup_data[0].$seat_data->seat_data), -15));

		if($lookup_data[2] == $confirmation_hash) {
			$mysql_query = "SELECT * FROM transactions WHERE confirmation_id='".mysql_real_escape_string($seat_data->seat_data)."' LIMIT 1";
			$transaction_data = $this->database->fetchOneRow($mysql_query);
			$return_array = array();
			$return_array["status"] = "STATUS_OK";
			$return_array["section"] = $lookup_data[0];
			$return_array["seatnumber"] = $lookup_data[1];
			$return_array["confirmationcode"] = $seat_data->seat_data;
			if($transaction_data->middle_name !== "") {
				$return_array["ticketowner"] = $transaction_data->first_name." ".$transaction_data->middle_name." ".$transaction_data->last_name;
			} else {
				$return_array["ticketowner"] = $transaction_data->first_name." ".$transaction_data->last_name;
			}
			$return_array["time"] = $transaction_data->transaction_time;
			$return_array["address1"] = $transaction_data->address_1;
			$return_array["city"] = $transaction_data->city;
			$return_array["state"] = $transaction_data->state;
			$return_array["zipcode"] = $transaction_data->zipcode;
			$return_array["email"] = $transaction_data->email;
			$return_array["tickettype"] = $seat_data->seat_type;
		} else {
			$return_array = array();
			$return_array["status"] = "STATUS_NOTOK";
		}

		echo json_encode($return_array);
	}
}

?>