<?php

/**
 * /participate/ URL Handler and Logic Controller
 *
 * Copyright (c) 2013 Ryan Tse, Mark Tai.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Participate extends Pages {
	public function __construct() {
		//Load both templating and session engines.
		parent::__construct(true, true);
		
		//Setup payment API key.
		$payment_configuration = Configuration::open("PAYMENT");
		Stripe::setApiKey($payment_configuration->private_apikey);

		//Setup seating manager.
		$this->seating_manager = new SeatingController();

		//Database.
		$database_config = Configuration::open("DATABASE");
		$this->database = MySqlDatabase::getInstance();
		$this->database->connect($database_config->host, $database_config->username, $database_config->password, $database_config->database);
	}

	public function selectseating() {
		//Show the select seating page.
		$this->template->display("registration/selectseating.tpl", $this->template_data);
	}

	public function registrationinfo() {
		//Require the seating to be available.
		$this->procedure_require_seats();

		//Setup payment API key and show CC form.
		$payment_configuration = Configuration::open("PAYMENT");
		$this->template_data->assign("show_cc_form", true);

		//Input verification logic.
		if($_SERVER["REQUEST_METHOD"]=="POST" && isset($_POST["authd"])) {

			//Add validation rules.
			$validator = new InputRules(&$_POST);
			$validator->addpreprocessor("registration_firstname", array("RegistrationRules", "FormatSpaces"));
			$validator->add("registration_firstname", array("RegistrationRules", "RequiredField"), "Your first name is required.");
			$validator->add("registration_firstname", array("RegistrationRules", "ValidateName"), "Your first name cannot contain numbers.");
			$validator->addpreprocessor("registration_middlename", array("RegistrationRules", "FormatSpaces"));
			$validator->addoptional("registration_middlename", array("RegistrationRules", "ValidateName"), "Your middle name cannot contain numbers.");
			$validator->addpreprocessor("registration_lastname", array("RegistrationRules", "FormatSpaces"));
			$validator->add("registration_lastname", array("RegistrationRules", "RequiredField"), "Your last name is required.");
			$validator->add("registration_lastname", array("RegistrationRules", "ValidateName"), "Your last name cannot contain numbers.");
			$validator->addpreprocessor("registration_addressline1", array("RegistrationRules", "FormatSpaces"));
			$validator->add("registration_addressline1", array("RegistrationRules", "RequiredField"), "Your address is required.");
			$validator->add("registration_addressline1", array("RegistrationRules", "ValidateAddress1"), "A valid address must be specified.");
			$validator->addpreprocessor("registration_city", array("RegistrationRules", "FormatSpaces"));
			$validator->add("registration_city", array("RegistrationRules", "RequiredField"), "Your city is required.");
			$validator->add("registration_state", array("RegistrationRules", "RequiredField"), "Your state is required.");
			$validator->add("registration_state", array("RegistrationRules", "ValidateState"), "A valid state must be specified.");
			$validator->addpreprocessor("registration_zipcode", array("RegistrationRules", "RemoveSpaces"));
			$validator->add("registration_zipcode", array("RegistrationRules", "RequiredField"), "A zip code is required.");
			$validator->add("registration_zipcode", array("RegistrationRules", "ValidateZipCode"), "A valid zip code must be specified.");
			$validator->addpreprocessor("registration_email", array("RegistrationRules", "RemoveSpaces"));
			$validator->add("registration_email", array("RegistrationRules", "RequiredField"), "An email address is required.");
			$validator->add("registration_email", array("RegistrationRules", "ValidateEmail"), "A valid email address is required.");

			//Only check if payment autenticatior is NOT set.
			if(!isset($_SESSION["payment_authenticator"])) {
				$validator->add("payment_authenticator", array("RegistrationRules", "RequiredField"), "There was an error processing your payment, please verify your payment information.");
				$validator->add("payment_authenticator", array($this, "ValidateToken"), "There was an error processing your payment, please verify your payment information.");
			} else {
				$payment_token = Stripe_Token::retrieve($_SESSION["payment_authenticator"]);
				if($payment_token["used"]) {
					$_POST["payment_authenticator"] = $_SESSION["payment_authenticator"];
					$validator->add("payment_authenticator", array($this, "ValidateToken"), "There was an error processing your payment, please verify your payment information.");
				}
				unset($payment_token);
			}

			//Check that validation fields have all passed.
			if($validator->check()) {
				$_SESSION["registrationinfo_complete"] = true;
				header("Location: /participate/verify/");
			} else {
				//Something bad has occurred, show the user this information.
				$this->template_data->assign("error_data", json_encode($validator->geterrors()));
			}

			//Store registration information into session.
			$_SESSION["verify_registrationinfo"] = array(
				"registration_firstname" =>  $_POST["registration_firstname"],
				"registration_middlename" =>  $_POST["registration_middlename"],
				"registration_lastname" =>  $_POST["registration_lastname"],
				"registration_addressline1" =>  $_POST["registration_addressline1"],
				"registration_addressline2" =>  $_POST["registration_addressline2"],
				"registration_city" =>  $_POST["registration_city"],
				"registration_state" =>  $_POST["registration_state"],
				"registration_zipcode" =>  $_POST["registration_zipcode"],
				"registration_email" => $_POST["registration_email"],
				"registration_agreetos" => $_POST["tos_agreement"]
			);
		}

		//Check if payment authentication has already been set.
		if(isset($_SESSION["payment_authenticator"])) {
			$this->template_data->assign("show_cc_form", false);
		}

		//Page display logic.
		$this->template_data->assign("payment_apikey", $payment_configuration->public_apikey);
		$this->template->display("registration/registrationinfo.tpl", $this->template_data);
	}

	public function registrationreset() {
		try {
			unset($_SESSION["payment_authenticator"]);
			unset($_SESSION["registrationinfo_complete"]);
		} catch (Exception $e) {}
		header("Location: /participate/registrationinfo");
	}

	public function terms() {
		$this->template->display("registration/terms.tpl", $this->template_data);
	}

	public function privacy() {
		$this->template->display("registration/privacy.tpl", $this->template_data);
	}

	public function verify() {
		$this->procedure_require_seats();
		if(!isset($_SESSION["registrationinfo_complete"])) {
			header("Location: /participate/registrationinfo");
			die();
		}
		//Verify payment information is still valid.
		try {
			$payment_token = Stripe_Token::retrieve($_SESSION["payment_authenticator"]);
			if($payment_token["	used"]) {
				throw new Exception();
			}
		} catch (Exception $exception) {
			unset($_SESSION["payment_authenticator"]);
			header("Location: /participate/registrationinfo/");
		}

		//Assume from beginning.
		$this->template_data->assign("has_student_seats", false);

		//Checkout page generation
		$validseats = array();
		$seats_removed = false;
		$selected_seats = array();
		foreach($_SESSION["selected_seats"] as $section => $seats) {
			$array_sort = array_keys($seats);
			sort($array_sort);
			$selected_seats[$section] = $array_sort;
		}
		foreach($selected_seats as $section => $seats) {
			foreach($seats as $seat) {
				switch($this->seating_manager->get_seat_status($section, $seat)) {
					case SeatingController::SEAT_RESERVED:
						$mysql_query = "SELECT seat_data FROM seats WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($seat)."' LIMIT 1";
						$reservation_data = json_decode($this->database->fetchOne($mysql_query), true);
						if($reservation_data["reserved_by"] == $_SESSION["unique_id"]) {
							$validseats[$section.$seat] = array();
							$validseats[$section.$seat]["price"] = $this->seating_manager->get_seat_price($section, $seat);
							$validseats[$section.$seat]["description"] = $this->seating_manager->get_seat_description($section, $seat);
							$validseats[$section.$seat]["section"] = $section;
							$validseats[$section.$seat]["seat"] = $seat;
							if($this->seating_manager->get_seat_type($section, $seat) == SeatingController::SEAT_STUDENT) {
								$this->template_data->assign("has_student_seats", true);
							}
						} else {
							unset($_SESSION["selected_seats"][$section][$seat]);
							$seats_removed = true;
						}
						break;

					case SeatingController::SEAT_PURCHASED:
						unset($_SESSION["selected_seats"][$section][$seat]);
						$seats_removed = true;
						break;

					case SeatingController::SEAT_OPEN:
						$validseats[$section.$seat] = array();
						$validseats[$section.$seat]["price"] = $this->seating_manager->get_seat_price($section, $seat);
						$validseats[$section.$seat]["description"] = $this->seating_manager->get_seat_description($section, $seat);
						$validseats[$section.$seat]["section"] = $section;
						$validseats[$section.$seat]["seat"] = $seat;
						if($this->seating_manager->get_seat_type($section, $seat) == SeatingController::SEAT_STUDENT) {
							$this->template_data->assign("has_student_seats", true);
						}
						break;
				}
			}
		}

		$checkout_total = 0;
		foreach($validseats as $key => $seat) {
			$checkout_total += $seat["price"];
		}

		if($_SERVER["REQUEST_METHOD"]=="POST" && isset($_POST["authd"])) {
			if(!$seats_removed) {
				if($checkout_total !== 0) {
					try {
						$charge_response = Stripe_Charge::create(array(
							"amount" => $checkout_total*100,
							"currency" => "usd",
							"card" => $_SESSION["payment_authenticator"],
							"description" => "Ticket purchase for Irvington 2013 Graduation"
						));
					} catch (Exception $exception) {
						unset($_SESSION["payment_authenticator"]);
						header("Location: /participate/registrationinfo");
						die();
					}
				} else {
					$charge_response = array();
					$charge_response["id"] = "nocharge_".strtolower(substr(hash("sha256",$_SESSION["verify_registrationinfo"]["registration_firstname"].rand(0,1234567890)), -15));
				}

				while(true) {
					$confirmation_id = strtoupper(substr(hash("sha256",$charge_response["id"].rand(0,9)), -8));
					$mysql_query = "SELECT COUNT(*) FROM transactions WHERE confirmation_id='".mysql_real_escape_string($confirmation_id)."'";
					if($this->database->fetchOne($mysql_query) > 0) {
						continue;
					} else {
						break;
					}
				}

				$mysql_query = "INSERT INTO transactions (first_name, middle_name, last_name, address_1, address_2, city, state, zipcode, email, transaction_token, confirmation_id) VALUES ('".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_firstname"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_middlename"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_lastname"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_addressline1"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_addressline2"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_city"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_state"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_zipcode"])."','".mysql_real_escape_string($_SESSION["verify_registrationinfo"]["registration_email"])."','".mysql_real_escape_string($charge_response["id"])."','".mysql_real_escape_string($confirmation_id)."')";
				$this->database->query($mysql_query);

				foreach($validseats as $seat_identifier => $seat_data) {
					$mysql_query = "UPDATE seats SET seat_status='PURCHASED', seat_data='".$confirmation_id."' WHERE section_id='".$seat_data["section"]."' AND seat_number='".$seat_data["seat"]."' LIMIT 1";
					$this->database->query($mysql_query);
				}

				$this->send_reservation_confirmation($confirmation_id);

				$_SESSION = array();
				$_SESSION["confirmation_id"] = $confirmation_id;
				header("Location: /participate/confirmation");
			}
		}

		if($seats_removed) {
			$this->template_data->assign("error_message", "One or more seats that you have selected are no longer available. They have been removed from your cart.");
		}
		$this->template_data->assign("checkout_total", $checkout_total);
		$this->template_data->assign("checkout_item_list", $validseats);
		$this->template_data->assign("payment_info", $payment_token["card"]);
		$this->template_data->assign("registration_info", $_SESSION["verify_registrationinfo"]);
		$this->template->display("registration/registrationverify.tpl", $this->template_data);
	}

	private function send_reservation_confirmation($confirmation_id) {

		//Load name and email information.
		$mysql_query = "SELECT first_name, middle_name, last_name, email, confirmation_id FROM transactions WHERE confirmation_id='".mysql_real_escape_string($confirmation_id)."' LIMIT 1";
		$result = $this->database->fetchOneRow($mysql_query);
		$full_name = $result->first_name;
		if($result->middle_name !== "") {
			$full_name .= " ".$result->middle_name;
		}
		$full_name .= " ".$result->last_name;
		$email = $result->email;

		//Find all associated tickets.
		$mysql_query = "SELECT section_id, seat_number, seat_type FROM seats WHERE seat_data='".mysql_real_escape_string($confirmation_id)."'";
		$seat_results = $this->database->query($mysql_query);
		$ticket_data = array();
		while($row = mysql_fetch_assoc($seat_results)) {
			$ticket_data[] = array(
				"ticket_type" => $row["seat_type"],
				"seat_section" => $row["section_id"],
				"seat_number" => $row["seat_number"],
				"confirmation_code" => $confirmation_id,
				"ticket_owner" => strtoupper($full_name)
			);
		}

		//Generate tickets.
		$ticket_controller = new TicketController();
		$ticket_file_data = $ticket_controller->generate_tickets($ticket_data, "S");

		//Prepare email.
		$email_configuration = Configuration::open("EMAIL");
		$email_data = $this->template->createData();
		$email_data->assign("confirmation_code", $confirmation_id);
		$mailer = new PHPMailer();
		$mailer->AddAddress($email, $full_name);
		$mailer->AddStringAttachment($ticket_file_data, "tickets.pdf", "base64", "application/pdf");
		$mailer->Subject = "IHS Graduation 2013 - Confirmation";
		$mailer->AltBody = "To view this email, please use an HTML enabled email client.";
		$mailer->MsgHTML($this->template->fetch("confirmation/email.tpl", $email_data));
		$mailer->SetFrom($email_configuration->email, $email_configuration->name);
		$mailer->XMailer = "IHS Graduation";
		$mailer->Send();
	}

	public function confirmation() {
		if(!isset($_SESSION["confirmation_id"])) {
			header("Location: /participate/verify/");
		}

		$confirmation_id = $_SESSION["confirmation_id"];
		unset($_SESSION);
		$_SESSION = array();
		session_destroy();

		$this->template_data->assign("confirmation_id", $confirmation_id);
		$this->template->display("registration/confirmation.tpl", $this->template_data);
	}

	public function ValidateToken($token) {
		try {
			$payment_token = Stripe_Token::retrieve($token);
			if($payment_token["used"]) {
				unset($_SESSION["payment_authenticator"]);
				throw new Exception();
				
			}
			$_SESSION["payment_authenticator"] = $token;
			return true;
		} catch (Exception $exception) {
			return false;
		}
	}

	private function procedure_require_seats() {
		$count = 0;
		foreach($_SESSION["selected_seats"] as $section => $seats) {
			$count += count($seats);
		}
		if($count == 0) {
			header("Location: /participate");
		}
	}
}