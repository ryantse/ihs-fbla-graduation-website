<?php

/**
 * Seating URL Handler and Logic Controller
 *
 * Copyright (c) 2013 Mark Tai, Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Seating extends Pages {
	public function __construct() {
		//Do not load templates, load a new session.
		parent::__construct(false, true);
		$this->seating_manager = new SeatingController();
	}

	public function full_data($width, $height) {
		echo json_encode($this->seating_manager->get_sections_scaled($width, $height));
		//Return coordinates of the sections
	}

	public function section_data($width, $height, $section) {
		echo json_encode($this->seating_manager->get_section_scaled($width, $height, $section));
		//Return the coordinates of the seats and status.
	}

	/**
	 * Reservation response layout
	 * 
	 * status: returns STATUS_OK or STATUS_NOTOK
	 * error_message: returns string based error message
	 * should_refresh: returns true or false based on if the map should be refreshed
	 */

	public function unreserve($section, $seat) {
		$return_array = array();
		$seat_unreserve_return = $this->seating_manager->unreserve_seat($_SESSION["unique_id"], $section, $seat);
		unset($_SESSION["selected_seats"][$section][$seat]);
		switch($seat_unreserve_return) {
			case SeatingController::ACTION_SUCCESS:
			case SeatingController::ACTION_FAILURE:
				$return_array["status"] = "STATUS_OK";
				$return_array["error_message"] = null;
				$return_array["should_refresh_view"] = true;
				$return_array["should_refresh_selection"] = true;
				break;
		}
		echo json_encode($return_array);
	}

	public function reserve($section, $seat) {
		$valid_sections = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
		$return_array = array();
		$seat_reserve_return = $this->seating_manager->reserve_seat($_SESSION["unique_id"], $section, $seat);
		switch($seat_reserve_return) {
			case SeatingController::ACTION_SUCCESS:
				$_SESSION["selected_seats"][$section][$seat] = true;
				ksort($_SESSION["selected_seats"]);
				foreach($valid_sections as $section) {
					if(isset($_SESSION[$section]) && count($_SESSION[$section]) > 0) {
						$seats = array_keys($_SESSION[$section]);
						$_SESSION[$section] = array();
						foreach($seats as $seat) {
							$_SESSION[$section][$seat] = true;
						}
					}
				}
				$return_array["status"] = "STATUS_OK";
				$return_array["error_message"] = null;
				$return_array["should_refresh_view"] = false;
				$return_array["should_refresh_selection"] = true;
				break;

			case SeatingController::ACTION_FAILURE:
				$return_array["status"] = "STATUS_NOTOK";
				switch($this->seating_manager->get_seat_status($section, $seat)) {
					case SeatingController::SEAT_RESERVED:
						$return_array["error_message"] = "The seat you requested has already been reserved; however, this seat may be available at a later time.";
						$return_array["should_refresh_view"] = true;
						$return_array["should_refresh_selection"] = false;
						break;

					case SeatingController::SEAT_PURCHASED:
						$return_array["error_message"] = "Sorry, it appears the seat you selected has already been purhcased. Please select another seat.";
						$return_array["should_refresh_view"] = true;
						$return_array["should_refresh_selection"] = false;
						break;

					case SeatingController::SEAT_NOTFOUND:
					case SeatingController::SEAT_OPEN:
						$return_array["error_message"] = "An error occurred trying to process your request. Please try again.";
						$return_array["should_refresh_view"] = true;
						$return_array["should_refresh_selection"] = true;
						break;
				}
				break;
		}
		echo json_encode($return_array);
	}

	public function select_list() {
		$valid_sections = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");

		$seating_list = array();
		if(isset($_SESSION["selected_seats"]) && count($_SESSION["selected_seats"]) > 0) {
			foreach($_SESSION["selected_seats"] as $section => $seats) {
				$array_sort = array_keys($seats);
				sort($array_sort);
				$seating_list[$section] = $array_sort;
			}
		}
			
		$return_array = array();
		foreach($valid_sections as $section) {
			if(isset($seating_list[$section]) && count($seating_list[$section]) > 0) {
				$return_array[$section] = $seating_list[$section];
			} else {
				$return_array[$section] = array();
			}
		}
		echo json_encode($return_array);
	}
}

?>