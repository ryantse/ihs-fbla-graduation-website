<?php

/**
 * Static URL Handler and Logic Controller
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Landing extends Pages {
	public function __construct() {
		//Load templating engine, do not load a new session.
		parent::__construct(true, false);
	}

	public function index() {
		//Display index page.
		$this->template->display("index.tpl");
	}

	public function notfound() {
		header("Location: /");
	}

	public function favicon() {
		header("Location: /static/img/icon1.ico", 302);
	}
}

?>