<?php

/**
 * URL Routing Table
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

//Home Page
Router::add('/','landing/index');
Router::add('/favicon.ico/', 'landing/favicon');

//Registration Pages
Router::add(array(
	'/participate/' => 'participate/selectseating',
	'/participate/selectseating/' => 'participate/selectseating',
	'/participate/registrationinfo/' => 'participate/registrationinfo',
	'/participate/registrationinfo/billingreset/' => 'participate/registrationreset',
	'/participate/terms/' => 'participate/terms',
	'/participate/privacy/' => 'participate/privacy',
	'/participate/verify/' => 'participate/verify',
	'/participate/confirmation/' => 'participate/confirmation'
));

//Seating Data
Router::add(array(
	'/participate/seating/full/data/(:num)/(:num)/' => 'seating/full_data/$1/$2',
	'/participate/seating/section/data/(:alpha)/(:num)/(:num)/' => 'seating/section_data/$2/$3/$1',
	'/participate/seating/reserve/(:alpha)/(:num)/' => 'seating/reserve/$1/$2',
	'/participate/seating/unreserve/(:alpha)/(:num)/' => 'seating/unreserve/$1/$2',
	'/participate/seating/reserve/all/' => 'seating/select_list'
));

//Reservation Lookup
Router::add(array(
	'/reservations/' => 'reservations/index',
	'/reservations/tickets.pdf/' => 'reservations/tickets'
));

//Reservation Verification
Router::add(array(
	'/verification/' => 'verification/index',
	'/verification/data/' => 'verification/lookup'
));

//Not Found (LAST, CATCH ALL)
Router::add('/(:any)', 'landing/notfound');

?>