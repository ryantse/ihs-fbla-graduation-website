<?php

/**
 * Seating Controller
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class SeatingController {

	const SEAT_NOTFOUND = 1;
	const SEAT_OPEN = 2;
	const SEAT_RESERVED = 4;
	const SEAT_PURCHASED = 8;

	const SEAT_STANDARD = 1;
	const SEAT_STUDENT = 0;

	const ACTION_SUCCESS = 0;
	const ACTION_FAILURE = 1;

	private $seating_properties;

	public function __construct() {
		$x = 10;
		$y = 10;
		$this->seating_properties = array(
			"A" => array($x,$y, "Students"), 
			"B" => array($x,$y, "Students"), 
			"C" => array($x,$y, "Students"), 
			"D" => array($x,$y, "Students"), 
			"E" => array($x,$y, "Guests"), 
			"F" => array($x,$y, "Guests"), 
			"G" => array($x,$y, "Guests"), 
			"H" => array($x,$y, "Guests"), 
			"I" => array($x,$y, "Guests"), 
			"J" => array($x,$y, "Guests"),
			"K" => array($x,$y, "Guests"),
			"L" => array($x, $y, "Guests"),
			"STAGE" => array(2*$x +1, $y/2, "STAGE")
		);

		$database_config = Configuration::open("DATABASE");
		$this->database = MySqlDatabase::getInstance();
		$this->database->connect($database_config->host, $database_config->username, $database_config->password, $database_config->database);
	} 

	public function get_sections_scaled($width, $height) {
		$section_arrangement = array(array("L", "A", "B", "E"), array("K", "C", "D", "F"), array("J", "I", "H", "G"));
		$section_width = count($section_arrangement[0]);
		$section_height = count($section_arrangement);
		$extra_x = $width % (1 +  $section_width * ($this->seating_properties["A"][0] + 1));
		$extra_y = $height % (2 + $section_height * ($this->seating_properties["A"][1] + 1) + $this->seating_properties["STAGE"][1]);
		$x = ($width - $extra_x)/ (1 +  $section_width * ($this->seating_properties["A"][0] + 1));
		$y = ($height - $extra_y)/ (2 + $section_height * ($this->seating_properties["A"][1] + 1) + $this->seating_properties["STAGE"][1]);
		$left_margin = floor($extra_x/2);
		$top_margin = floor($extra_y/2);
		$section_area = array();
		$section_position_x = 1 * ($this->seating_properties["A"][0] + 1) * $x + $left_margin;
		$section_position_y = 1 * $y + $top_margin;
		$section = "STAGE";
		$section_area[$section]= array($section_position_x, $section_position_y, $this->seating_properties[$section][0] * $x + $section_position_x, $this->seating_properties[$section][1] * $y + $section_position_y);
		$section_area[$section][] = implode("<br />", array($section));
		$top_margin += ($this->seating_properties[$section][1] + 1)* $y + $section_position_y;
		$section_y = 0;
		while ($section_y < $section_height) {
			$section_x = 0;
			$section_position_y = $section_y * ($this->seating_properties["A"][1] + 1) * $y+ $top_margin;
			while ($section_x < $section_width) {
				$section_position_x = $section_x * ($this->seating_properties["A"][0] + 1) * $x + $left_margin;
				$section = $section_arrangement[$section_y][$section_x];
				
				$section_area[$section]= array($section_position_x, $section_position_y, $this->seating_properties[$section][0] * $x + $section_position_x, $this->seating_properties[$section][1] * $y + $section_position_y);
				$section_statuses = array_count_values($this->get_section_status($section));
				$section_area[$section][] = implode("<br />", array($section, $this->seating_properties[$section][2], intval($section_statuses[self::SEAT_OPEN]). " open"));
		
				$section_x+= 1;
			}
			$section_y += 1;
		}
		return $section_area;
	}

	public function get_section_scaled($width, $height, $section) {
		$seat_width = 10;
		$seat_height = 10;
		$section = strtoupper($section);
		$row_width = $this->seating_properties[$section][0];
		$column_height = $this->seating_properties[$section][1];
		$extra_x = $width % ((1+$seat_width)*($row_width) + 1);
		$extra_y = $height % ((1+$seat_height)*($column_height) + 1);
		$x = ($width - $extra_x)/  ((1+$seat_width)*($row_width) + 1);
		$y = ($height - $extra_y)/ ((1+$seat_height)*($column_height) + 1);
		$left_margin = floor($extra_x/2) + $x;
		$top_margin = floor($extra_y/2) + $y;
		$seat_x = 1;
		$seat_array = array();
		$section_status = $this->get_section_status($section);
		while ($seat_x <= $row_width) {
			$seat_y = 1;
			$seat_position_x =  floor((1+$seat_width)*$x*($seat_x-1));
			while ($seat_y <= $column_height) {
				$seat_number = $row_width * ($seat_y - 1) + $seat_x;
				$seat_position_y = floor((1+$seat_height)*$y*($seat_y-1));
				switch($section_status[$seat_number]) {
					case self::SEAT_OPEN:
						$seat_status = 0;
						break;

					default:
						$seat_status = 1;
						break;
				}
				$seat_array[$seat_number] = array($seat_position_x + $left_margin, $seat_position_y + $top_margin, $seat_position_x + $seat_width*$x + $left_margin, $seat_position_y + $seat_height *$y + $top_margin, $seat_number, $seat_status);
				$seat_y += 1;
			}
			$seat_x += 1;
		}
		return $seat_array;
	}

	public function reserve_seat($identifier, $section, $seat_number) {
		switch($this->seat_status($section, $seat_number)) {
			case self::SEAT_OPEN:
					$reservation_data = array(
						"expires" => strtotime("+10 minutes"),
						"reserved_by" => $identifier
					);
					$mysql_query = "UPDATE seats SET seat_status='RESERVED', seat_data='".mysql_real_escape_string(json_encode($reservation_data))."' WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($seat_number)."' LIMIT 1";
					$this->database->query($mysql_query);
					return self::ACTION_SUCCESS;
				break;

			case self::SEAT_RESERVED:
				$mysql_query = "SELECT seat_data FROM seats WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($seat_number)."' LIMIT 1";
				$reservation_data = json_decode($this->database->fetchOne($mysql_query), true);
				if($reservation_data["expires"] < time()) {
					$this->unreserve_seat($reservation_data["reserved_by"], $section, $seat_number);
					$this->reserve_seat($identifier, $section, $seat_number);
					return self::ACTION_SUCCESS;
				} else {
					return self::ACTION_FAILURE;
				}
				break;

			case self::SEAT_PURCHASED:
			case self::SEAT_NOTFOUND:
				return self::ACTION_FAILURE;
				break;
		}
		return self::ACTION_FAILURE;
	}

	public function unreserve_seat($identifier, $section, $seat_number) {
		switch($this->seat_status($section, $seat_number)) {
			case self::SEAT_PURCHASED:
			case self::SEAT_NOTFOUND:
			case self::SEAT_OPEN:
				return self::ACTION_FAILURE;
				break;

			case self::SEAT_RESERVED:
				$mysql_query = "SELECT seat_data FROM seats WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($seat_number)."' LIMIT 1";
				$reservation_data = json_decode($this->database->fetchOne($mysql_query), true);
				if($reservation_data["reserved_by"] == $identifier) {
					$mysql_query = "UPDATE seats SET seat_status='OPEN', seat_data='' WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($seat_number)."' LIMIT 1";
					$this->database->query($mysql_query);
					return self::ACTION_SUCCESS;
				} else {
					return self::ACTION_FAILURE;
				}
				break;
		}
		return self::ACTION_FAILURE;
	}

	public function get_seat_status($section, $seat_number) {
		return $this->seat_status($section, $seat_number);
	}

	public function get_seat_price($section, $seat_number) {
		switch($this->get_seat_type($section, $seat_number)) {
			case self::SEAT_STANDARD:
				return 10;
				break;

			case self::SEAT_STUDENT:
				return 0;
				break;
		}
	}

	public function get_seat_description($section, $seat_number) {
		switch($this->get_seat_type($section, $seat_number)) {
			case self::SEAT_STANDARD:
				return "Standard seating, for parents and other guests.";
				break;

			case self::SEAT_STUDENT:
				return "Reserved student seat, for graduating seniors only.";
				break;
		}
	}

	public function get_seat_type($section, $seat_number) {
		$mysql_query = "SELECT seat_type FROM seats WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($seat_number)."' LIMIT 1";
		switch($this->database->fetchOne($mysql_query)) {
			case "STANDARD":
				return self::SEAT_STANDARD;
				break;

			case "STUDENT":
				return self::SEAT_STUDENT;
				break;
		}
	}

	private function separate_seat($seat) {
		$len = strlen($seat);
		$chars = "";
		$number = "";
		for ($pos = 0; $pos < $len; $pos++) {
			$let = substr($seat, $pos,1);
			if (is_numeric($let)) {
				$number = $number . $let;
			}
			else {
				$chars = $chars . $let;
			}
		}
		return array($chars, $number);
	}

	private function seat_status() {
		if(func_num_args() == 2) {
			$section = func_get_arg(0);
			$seat_number = func_get_arg(1);
		} else if (func_num_args() == 1) {
			list($section, $seat_number) = $this->separate_seat(func_get_arg(0));
		}

		$mysql_query = "SELECT seat_status FROM seats WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($seat_number)."'";
		$seat_status = $this->database->fetchOne($mysql_query);

		switch ($seat_status) {
			case 'OPEN':
				return self::SEAT_OPEN;
				break;
			
			case 'RESERVED':
				return self::SEAT_RESERVED;
				break;

			case 'PURCHASED':
				return self::SEAT_PURCHASED;
				break;

			default:
				return self::SEAT_NOTFOUND;
				break;
		}
	}

	private function get_section_status($section){
		$return_array = array();
		$section = strtoupper($section);
		$mysql_query = "SELECT seat_number, seat_status FROM seats WHERE section_id='".mysql_real_escape_string($section)."'";
		$result = $this->database->query($mysql_query);
		while($row = mysql_fetch_assoc($result)) {
			switch ($row["seat_status"]) {
				case 'OPEN':
					$return_array[$row["seat_number"]] = self::SEAT_OPEN;
					break;
				
				case 'RESERVED':
					$mysql_query = "SELECT seat_data FROM seats WHERE section_id='".mysql_real_escape_string($section)."' AND seat_number='".mysql_real_escape_string($row["seat_number"])."' LIMIT 1";
					$reservation_data = json_decode($this->database->fetchOne($mysql_query), true);
					if($reservation_data["expires"] < time()) {
						$this->unreserve_seat($reservation_data["reserved_by"], $section, $row["seat_number"]);
						$return_array[$row["seat_number"]] = self::SEAT_OPEN;
					} else {
						$return_array[$row["seat_number"]] = self::SEAT_RESERVED;
					}
					break;

				case 'PURCHASED':
					$return_array[$row["seat_number"]] = self::SEAT_PURCHASED;
					break;
			}
		}

		return $return_array;
	}
}

?>