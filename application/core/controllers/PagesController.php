<?php

/**
 * Basis For All Pages
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class Pages {
	private $session_started = false;

	public function __construct($templating = true, $sessions = true) {
		if($templating) $this->InitializeTemplates();
		if($sessions) $this->InitializeSession();
	}

	private function InitializeTemplates() {
		$this->template = new Smarty();
		$this->template->setUseSubDirs(TRUE);

		$this->template->setTemplateDir(constant("__APPDIR__")."/templates/");
		$this->template->setCompileDir(constant("__APPDIR__")."/templates/compiled/");
		$this->template->setCacheDir(constant("__APPDIR__")."/templates/cached/");
		$this->template->setConfigDir(constant("__APPDIR__")."/templates/configs/");

		$this->template_data = $this->template->createData();
	}

	private function InitializeSession() {
		if(!$this->session_started) {
			$application_config = Configuration::open("APPLICATION");
			session_name(hash("sha256", $application_config->security_token));
			session_start();

			if(!isset($_SESSION["unique_id"])) {
				$_SESSION["unique_id"] = uniqid("", true);
			}
		}
		$this->session_started = true;
	}
}

?>