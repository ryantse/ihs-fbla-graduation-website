<?php

/**
 * Ticket Controller
 *
 * Copyright (c) 2013 Ryan Tse, Mark Tai.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class TicketController {

	public function generate_tickets($ticket_data, $output_type = "I") {
		$last_page_ticketnumber = count($ticket_data) % 3;
		$number_of_regularpages = (count($ticket_data) - $last_page_ticketnumber)/3;
		$current_ticket = 0;

		$qr_style = array(
			'border' => 2,
			'vpadding' => 'auto',
			'hpadding' => 'auto',
			'fgcolor' => array(0,0,0),
			'bgcolor' => false,
			'module_width' => 1,
			'module_height' => 1
		);

		$pdf_controller = new TCPDF();
		$pdf_controller->SetMargins(0, PDF_MARGIN_TOP, 0);
		$pdf_controller->SetAutoPageBreak(false, 0);
		$pdf_controller->SetFontSubsetting(false);
		$pdf_controller->setPrintHeader(false);
		$pdf_controller->setPrintFooter(false);

		$border_style = array("all" => array(
			"width" => "0.25",
			"cap" => "square",
			"join" => "mitter",
			"dash" => 0,
			"phase" => 0
		));

		$no_border = array("all" => array(
			"width" => "0",
			"cap" => "square",
			"join" => "mitter",
			"dash" => 0,
			"phase" => 0
		));

		if($number_of_regularpages > 0) {
			for($current_page = 1; $current_page <= $number_of_regularpages; $current_page++) {
				$pdf_controller->AddPage();
				for($i = 0; $i < 3; $i++) {
					//Outer box
					$pdf_controller->SetFillColor(255, 255, 255);
					$pdf_controller->SetDrawColor(0, 0, 0);
					$pdf_controller->Rect(25.25, 36.37+72.56*($i), 165.10, 61.69, "DF", $border_style);

					//IHS Graduation 2013
					$pdf_controller->SetFont("helvetica", "", 28);
					$pdf_controller->SetXY(29.91, 38+72.56*($i));
					$pdf_controller->Write(5, "IHS Graduation 2013");

					//Ticket titling
					$pdf_controller->SetFont("helvetica", "", 10);
					$pdf_controller->SetXY(29.44, 53.18+72.56*($i));
					$pdf_controller->Write(5, "E-TICKET - ADMIT ONE");
					$pdf_controller->SetXY(29.44, 60.06+72.56*($i));
					$pdf_controller->Write(5, "Ticket Type:");
					$pdf_controller->SetXY(29.44, 70.60+72.56*($i));
					$pdf_controller->Write(5, "Ticket Owner:");
					$pdf_controller->SetXY(29.44, 85.79+72.56*($i));
					$pdf_controller->Write(5, "TICKET ISSUED AT:");

					//Section and seating boxes
					$pdf_controller->SetDrawColor(255, 255, 255);
					$pdf_controller->SetFillColor(166, 166, 166);
					$pdf_controller->Rect(76.78, 51.70+72.56*($i), 28.49, 33.00, "DF", $no_border);
					$pdf_controller->Rect(111.41, 51.70+72.56*($i), 28.46, 33.00, "DF", $no_border);

					//Section and seating labels
					$pdf_controller->SetFont("helvetica", "", 10);
					$pdf_controller->SetXY(82.25, 77.63+72.56*($i));
					$pdf_controller->Write(5, "SECTION");
					$pdf_controller->SetXY(112.01, 77.63+72.56*($i));
					$pdf_controller->Write(5, "SEAT NUMBER");
					$pdf_controller->SetXY(146.02, 84.04+72.56*($i));
					$pdf_controller->Write(5, "CONFIRMATION CODE:");
					
					//Ticket information
					$pdf_controller->SetFont("helvetica", "", 12);
					$pdf_controller->SetXY(30, 65+72.56*($i));
					$pdf_controller->Write(5, $ticket_data[$current_ticket]["ticket_type"]);
					$pdf_controller->SetXY(30, 76+72.56*($i));
					$pdf_controller->Write(5, $ticket_data[$current_ticket]["ticket_owner"]);
					$pdf_controller->SetXY(30, 91+72.56*($i));
					$pdf_controller->Write(5, date(DATE_RFC822));
					$pdf_controller->SetXY(155.03, 89.04+72.56*($i));
					$pdf_controller->Write(5, $ticket_data[$current_ticket]["confirmation_code"]);
					
					//Seating and section number
					$pdf_controller->SetFont("helvetica", "", 50);
					$section_location = $this->center_text_in_box($pdf_controller, 76.78, 28.49, $ticket_data[$current_ticket]["seat_section"]);
					$pdf_controller->SetXY($section_location-0.93, 54.70+72.56*($i));
					$pdf_controller->Write(5, $ticket_data[$current_ticket]["seat_section"]);
					$seat_location = $this->center_text_in_box($pdf_controller, 111.41, 28.46, $ticket_data[$current_ticket]["seat_number"]);
					$pdf_controller->SetXY($seat_location-0.9, 54.70+72.56*($i));
					$pdf_controller->Write(5, $ticket_data[$current_ticket]["seat_number"]);

					//QR Code
					$application = Configuration::open("APPLICATION");
					$hash = strtoupper(substr(hash("sha256",$application->securitytoken.$ticket_data[$current_ticket]["seat_number"].$ticket_data[$current_ticket]["seat_section"].$ticket_data[$current_ticket]["confirmation_code"]), -15));
					$pdf_controller->write2DBarcode($ticket_data[$current_ticket]["seat_section"]."|".$ticket_data[$current_ticket]["seat_number"]."|".$hash, "QRCODE,H", 149.60, 50+72.56*($i), 34.55, 34.55, $qr_style, "N");

					$current_ticket++;
				}
			}
		}

		if($last_page_ticketnumber !== 0) {
			$pdf_controller->AddPage();
			for($i = 0; $i < $last_page_ticketnumber; $i++) {
				//Outer box
				$pdf_controller->SetFillColor(255, 255, 255);
				$pdf_controller->SetDrawColor(0, 0, 0);
				$pdf_controller->Rect(25.25, 36.37+72.56*($i), 165.10, 61.69, "DF", $border_style);

				//IHS Graduation 2013
				$pdf_controller->SetFont("helvetica", "", 28);
				$pdf_controller->SetXY(29.91, 38+72.56*($i));
				$pdf_controller->Write(5, "IHS Graduation 2013");

				//Ticket titling
				$pdf_controller->SetFont("helvetica", "", 10);
				$pdf_controller->SetXY(29.44, 53.18+72.56*($i));
				$pdf_controller->Write(5, "E-TICKET - ADMIT ONE");
				$pdf_controller->SetXY(29.44, 60.06+72.56*($i));
				$pdf_controller->Write(5, "Ticket Type:");
				$pdf_controller->SetXY(29.44, 70.60+72.56*($i));
				$pdf_controller->Write(5, "Ticket Owner:");
				$pdf_controller->SetXY(29.44, 85.79+72.56*($i));
				$pdf_controller->Write(5, "TICKET ISSUED AT:");

				//Section and seating boxes
				$pdf_controller->SetDrawColor(255, 255, 255);
				$pdf_controller->SetFillColor(166, 166, 166);
				$pdf_controller->Rect(76.78, 51.70+72.56*($i), 28.49, 33.00, "DF", $no_border);
				$pdf_controller->Rect(111.41, 51.70+72.56*($i), 28.46, 33.00, "DF", $no_border);

				//Section and seating labels
				$pdf_controller->SetFont("helvetica", "", 10);
				$pdf_controller->SetXY(82.25, 77.63+72.56*($i));
				$pdf_controller->Write(5, "SECTION");
				$pdf_controller->SetXY(112.01, 77.63+72.56*($i));
				$pdf_controller->Write(5, "SEAT NUMBER");
				$pdf_controller->SetXY(146.02, 84.04+72.56*($i));
				$pdf_controller->Write(5, "CONFIRMATION CODE:");
				
				//Ticket information
				$pdf_controller->SetFont("helvetica", "", 12);
				$pdf_controller->SetXY(30, 65+72.56*($i));
				$pdf_controller->Write(5, $ticket_data[$current_ticket]["ticket_type"]);
				$pdf_controller->SetXY(30, 76+72.56*($i));
				$pdf_controller->Write(5, $ticket_data[$current_ticket]["ticket_owner"]);
				$pdf_controller->SetXY(30, 91+72.56*($i));
				$pdf_controller->Write(5, date(DATE_RFC822));
				$pdf_controller->SetXY(155.03, 89.04+72.56*($i));
				$pdf_controller->Write(5, $ticket_data[$current_ticket]["confirmation_code"]);
				
				//Seating and section number
				$pdf_controller->SetFont("helvetica", "", 50);
				$section_location = $this->center_text_in_box($pdf_controller, 76.78, 28.49, $ticket_data[$current_ticket]["seat_section"]);
				$pdf_controller->SetXY($section_location-0.93, 54.70+72.56*($i));
				$pdf_controller->Write(5, $ticket_data[$current_ticket]["seat_section"]);
				$seat_location = $this->center_text_in_box($pdf_controller, 111.41, 28.46, $ticket_data[$current_ticket]["seat_number"]);
				$pdf_controller->SetXY($seat_location-0.9, 54.70+72.56*($i));
				$pdf_controller->Write(5, $ticket_data[$current_ticket]["seat_number"]);

				//QR Code
				$application = Configuration::open("APPLICATION");
				$hash = strtoupper(substr(hash("sha256",$application->securitytoken.$ticket_data[$current_ticket]["seat_number"].$ticket_data[$current_ticket]["seat_section"].$ticket_data[$current_ticket]["confirmation_code"]), -15));
				$pdf_controller->write2DBarcode($ticket_data[$current_ticket]["seat_section"]."|".$ticket_data[$current_ticket]["seat_number"]."|".$hash, "QRCODE,H", 149.60, 50+72.56*($i), 34.55, 34.55, $qr_style, "N");

				$current_ticket++;
			}
		}

		return $pdf_controller->Output('tickets.pdf', $output_type);
	}

	private function center_text_in_box($pdf_controller, $x1, $box_width, $display_string) {
		//Enter dimensions of box, and a one lined text 
		if ($box_width < 1) {
			return false;
		}
		$text_x = $x1 + ($box_width/2) - ($pdf_controller->GetStringWidth($display_string)/2);
		
		return $text_x;
	}
}

?>