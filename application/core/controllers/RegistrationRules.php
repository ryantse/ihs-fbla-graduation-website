<?php

/**
 * Registration Rules for Input
 *
 * Copyright (c) 2013 Ryan Tse, Mark Tai, Krish Masand.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

class RegistrationRules {
	public static function RequiredField($value) {
			$value = preg_replace("/ /", "", $value);
		return (strlen($value) > 0);
	}
	
	public static function ValidateName($name) {
		// Checks for numbers in the string
		$length = strlen($name);
		for ($position = 0; $position < $length; $position++) {
			$letter = substr($name, $position,1);
			if (is_numeric($letter)) {
				return False;
			}
		}
		return True;
		
	}
	
	public static function ValidateAddress1 ($address) {
		//Checks the address
		$address = strtolower($address);
		$length = strlen($address);
		$letter_count = 0;
		$LETTERS = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
		$number_count = 0;
		$space_count = 0;
		for ($position = 0; $position < $length; $position++) {
			$character = substr($address, $position,1);
			if (is_numeric($character)) {
				$number_count += 1;
			}
			elseif ($character == " ") {
				$space_count += 1;
			}
			else {
				foreach ($LETTERS as $letter) {
					if ($character == $letter) {
						$letter_count += 1;
					}
				}
			}
		}
		if ($number_count >= 1 and $letter_count >= 1 and $space_count >= 1) {
			return True;
		}
		else {
			return False;
		}
	}

	public static function  ValidateState($state) {
		//Validates a United State
		$state = strtoupper($state);
		$STATES = array("AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY");
		foreach($STATES as $check){
			if ($state === $check) {
				return True;
			}
		}
		return False;
	}	
	
	public static function ValidateZipCode($zip) {
		//Checks for a valid zip code
		$length = strlen($zip);
		if ($length == 10 and substr($zip, 5, 1) == "-") {
			$zip = preg_replace("/-/", "", $zip, 1);
		}
		elseif  ($length != 5) {
			return False;
		}
		$length = strlen($zip);
		for ($position = 0; $position < $length; $position++) {
			$number = substr($zip, $position,1);
			if (!is_numeric($number)) {
				return False;
			}
		}
		return True;
	}
	
	public static function ValidateEmail($email) {
		//Checks for a valid email
		$email = strtolower($email); //creates a more concise list later
		$position = strpos($email, "@");
		if ($position === 0 or $position === strlen($email)-1 or !$position){
			return False;
		}
		$email = preg_replace("/@/", "", $email, 1);
		$length = strlen($email);
		$CHARACTERS = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".");
		for ($number = 0; $number < $length; $number++) {
			$char = substr($email, $number,1);
			foreach($CHARACTERS as $check){
				$bool = False;
				if ($char === $check) {
					$bool = True;
					break;
				}
			}
			if ($bool == False){
				return False;
			}
		}
		return True;
	}

	public static function RemoveSpaces($text) {
		$text = preg_replace("/ /", "", $text);
	}

	public static function FormatSpaces($text) {
		while (substr($text, 0, 1) == " "){
			$text = preg_replace("/ /", "", $text, 1);
		}
		while (substr($text, strlen($text) -1, 1) == " " ) {
			$text = substr($text, 0, strlen($text) - 1);
		}
		$text = preg_replace("/  /", " ", $text);
	}
}

// ?>