<?php

/**
 * Application Initialzation Script
 *
 * Copyright (c) 2013 Ryan Tse.
 *
 * NOTICE: If you add or change code in this file, add your name to
 * the copyright information above.
 */

//Must be called from the index.php file.
if(!defined("__APPDIR__")) { die(); }

//Load requisite libraries.
require(constant("__APPDIR__")."/core/libraries/Router/Router.class.php");
require(constant("__APPDIR__")."/core/libraries/Smarty/Smarty.class.php");
require(constant("__APPDIR__")."/core/libraries/FormVerification/FormVerification.class.php");
require(constant("__APPDIR__")."/core/libraries/Configuration/Configuration.class.php");
require(constant("__APPDIR__")."/core/libraries/Stripe/Stripe.php");
require(constant("__APPDIR__")."/core/libraries/MySQL/MySQLDatabase.class.php");
require(constant("__APPDIR__")."/core/libraries/MySQL/MySQLException.class.php");
require(constant("__APPDIR__")."/core/libraries/MySQL/MySQLResultSet.class.php");
require(constant("__APPDIR__")."/core/libraries/TCPDF/tcpdf.php");
require(constant("__APPDIR__")."/core/libraries/Mailer/mail.class.php");

//Load controllers and configuration.
require(constant("__APPDIR__")."/core/Configuration.php");
require(constant("__APPDIR__")."/core/controllers/RegistrationRules.php");
require(constant("__APPDIR__")."/core/controllers/PagesController.php");
require(constant("__APPDIR__")."/core/controllers/SeatingController.php");
require(constant("__APPDIR__")."/core/controllers/TicketController.php");

//Load routing table and begin routing.
require(constant("__APPDIR__")."/core/controllers/RoutingTable.php");
require(constant("__APPDIR__")."/core/controllers/RoutingDirector.php");

?>